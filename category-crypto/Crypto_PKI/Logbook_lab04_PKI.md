# Logbook for Lab 04 Public Key Infrastructure (PKI)

## Purpose


## Task 1: Becoming a Certificate Authority (CA)
First thing is to customize our openssl conf file.

```
[ CA_default ]

dir		= /volumes/lab04	# Where everything is kept - group 4 dir
certs		= $dir/certs		# Where the issued certs are kept
crl_dir		= $dir/crl		# Where the issued crl are kept
database	= $dir/index.txt	# database index file.
unique_subject	= no			# Set to 'no' to allow creation of
					# several certs with same subject.
new_certs_dir	= $dir/newcerts		# default place for new certs.

certificate	= $dir/cacert.pem 	# The CA certificate
serial		= $dir/serial 		# The current serial number
crlnumber	= $dir/crlnumber	# the current crl number
					# must be commented out to leave a V1 CRL
crl		= $dir/crl.pem 		# The current CRL
private_key	= $dir/cakey.pem	# The private key
```

Our generated certificate is shown below:

```bash
[04/08/22]seed@vm:~/.../lab04$ openssl x509 -in ca.crt -text -noout | cat -n
     1	Certificate:
     2	    Data:
     3	        Version: 3 (0x2)
     4	        Serial Number:
     5	            58:db:cb:18:9f:f8:be:b6:e6:61:dd:5a:fc:b6:ff:a6:54:b6:b9:f8
     6	        Signature Algorithm: sha256WithRSAEncryption
     7	        Issuer: CN = www.CAgroup7.com, O = Group7 CA LTD., C = US
     8	        Validity
     9	            Not Before: Apr  8 09:41:05 2022 GMT
    10	            Not After : Apr  5 09:41:05 2032 GMT
    11	        Subject: CN = www.CAgroup7.com, O = Group7 CA LTD., C = US
    12	        Subject Public Key Info:
    13	            Public Key Algorithm: rsaEncryption
    14	                RSA Public-Key: (4096 bit)
    15	                Modulus:
    16	                    00:dd:bb:68:b3:57:0b:9f:dd:d5:8c:3d:a2:1b:83:
    17	                    de:7b:bf:25:c0:29:ce:15:5d:fb:5e:a1:7e:e2:80:
    18	                    37:7e:61:28:1e:2b:c3:fe:9a:58:36:14:36:2c:2e:
    19	                    03:e2:51:78:3b:4c:3d:bc:eb:cc:b6:8b:29:3f:92:
    20	                    82:65:b1:9c:d0:f8:07:99:03:c8:35:8a:39:08:1d:
    21	                    02:ef:38:ba:a0:6b:3e:56:d8:90:8a:3a:23:28:b1:
    22	                    1e:83:9e:e1:90:36:dc:9d:fa:b0:aa:4b:2c:e3:6c:
    23	                    54:ee:24:73:29:a5:c4:1e:36:d0:5c:78:99:be:a8:
    24	                    ac:f3:e0:ed:de:f5:8a:ed:39:95:fc:ce:f6:34:57:
    25	                    e3:c8:41:26:4e:1b:c4:e6:c7:03:d8:71:30:0f:9f:
    26	                    e2:94:84:ef:7e:5c:dc:f0:3c:ab:79:61:b2:ae:f3:
    27	                    63:cc:13:24:56:a8:c5:e5:24:42:bd:75:98:0c:21:
    28	                    ba:83:b7:e8:a9:1c:bd:9e:93:69:db:8a:b9:5b:ea:
    29	                    b6:66:3b:24:5a:56:e5:49:6a:99:7a:b8:8a:2c:7e:
    30	                    d4:57:80:d6:c4:fc:f3:82:bb:fb:e6:81:4d:b2:db:
    31	                    1c:31:37:9b:d6:f1:ba:27:5f:f0:c2:44:cb:79:82:
    32	                    c2:44:41:83:a4:b2:3f:24:2f:32:08:0f:15:d7:62:
    33	                    15:08:33:9d:61:a0:b8:25:31:4d:1e:05:0d:44:68:
    34	                    03:7e:6d:c9:aa:01:a3:2f:b6:43:0b:a6:00:b9:22:
    35	                    d4:64:10:98:ff:11:bc:cb:5b:08:e0:44:9f:ce:ce:
    36	                    ed:87:23:86:2f:54:43:2e:a1:e1:7d:82:bb:3e:e7:
    37	                    5f:0c:69:c6:56:6d:2b:83:d3:51:59:5a:96:76:5e:
    38	                    bb:58:f8:3a:70:c7:e5:9d:f2:41:80:56:c1:52:2e:
    39	                    da:14:53:e3:9a:ec:44:b5:fe:ed:e1:14:25:fd:07:
    40	                    06:80:67:9b:9b:f4:0c:52:0e:ea:ca:a9:b3:7b:e2:
    41	                    9b:f1:89:d4:02:4b:cf:c9:9c:4c:70:89:a8:a5:0c:
    42	                    d6:5a:92:b6:52:6e:71:f2:69:46:aa:7e:53:a5:f1:
    43	                    09:0e:88:6c:97:5a:72:11:c2:1f:8f:4c:d7:89:fa:
    44	                    c9:11:4e:85:b6:be:e5:28:3b:72:8e:9e:36:dc:43:
    45	                    48:8f:36:13:00:da:3e:11:51:e0:ea:d5:9f:7c:5d:
    46	                    6f:51:f1:81:2e:21:78:6e:74:af:74:c8:3f:8a:47:
    47	                    ca:7a:dc:97:df:bd:3f:f7:21:ab:5c:31:1e:ff:be:
    48	                    31:85:40:60:4d:6d:6b:f1:45:ff:35:00:0c:0e:9d:
    49	                    b0:78:c6:bb:7c:f3:33:07:4f:fd:b6:5c:2f:cc:31:
    50	                    b6:b7:49
    51	                Exponent: 65537 (0x10001)
    52	        X509v3 extensions:
    53	            X509v3 Subject Key Identifier: 
    54	                A9:66:A7:C6:32:B2:EB:21:5C:11:AD:74:A7:FE:86:D2:9A:7E:BE:C5
    55	            X509v3 Authority Key Identifier: 
    56	                keyid:A9:66:A7:C6:32:B2:EB:21:5C:11:AD:74:A7:FE:86:D2:9A:7E:BE:C5
    57	
    58	            X509v3 Basic Constraints: critical
    59	                CA:TRUE
    60	    Signature Algorithm: sha256WithRSAEncryption
    61	         62:69:9b:86:36:65:0e:bb:1f:15:97:9f:e0:c3:ea:88:24:08:
    62	         80:9b:8f:73:14:7e:7d:3d:f0:3f:5f:b8:01:36:79:ea:b8:76:
    63	         1f:3a:5d:48:f8:9a:62:1d:09:e2:f7:e8:99:0d:92:7c:3d:62:
    64	         99:5d:c5:a3:ce:1a:10:1b:4b:9d:1f:6c:59:c3:e7:51:84:81:
    65	         aa:d4:90:83:d6:be:73:7d:57:8b:22:ca:6f:fd:5d:41:40:bb:
    66	         da:ef:aa:2b:8f:e6:46:3d:3c:d2:70:bd:ea:34:50:ad:72:3c:
    67	         72:68:52:d0:4a:94:26:70:3d:96:7d:89:ef:35:5c:86:c5:1e:
    68	         4b:4d:ad:4a:21:fe:2c:df:6e:d1:b9:e3:8a:9e:e5:c7:22:1d:
    69	         86:8d:97:7f:f7:bf:ab:74:87:4e:2c:5a:ed:e3:80:e9:aa:4d:
    70	         c0:ca:5f:2c:15:d7:49:1b:ed:ad:29:1d:36:14:96:8c:5f:86:
    71	         b5:d8:a2:94:9c:0f:b7:38:dc:56:0a:09:32:69:e3:c4:81:47:
    72	         3b:4a:c2:af:7c:e4:44:09:51:b5:32:a1:d0:7a:9c:2f:54:54:
    73	         21:0b:fb:72:7f:71:21:de:b3:a9:5b:98:b4:5c:30:ba:d2:14:
    74	         73:7f:8d:c4:bf:77:9f:b0:f1:c7:19:4f:40:1f:30:b0:9a:3b:
    75	         ec:48:46:dc:69:d7:14:4a:4b:cc:7e:5f:c3:fe:b5:71:9c:b4:
    76	         31:21:b5:4c:64:36:6a:50:0f:b1:23:50:6e:08:4b:60:d0:bf:
    77	         e3:6a:6a:52:a0:37:c5:cb:f3:2c:70:f6:6c:2d:11:ce:a3:75:
    78	         39:b4:98:e2:b5:55:6f:0c:70:fa:30:db:e5:31:eb:5f:d3:ae:
    79	         6e:41:87:88:78:af:2b:92:16:27:46:8c:54:95:b3:26:51:ef:
    80	         12:72:c0:e1:69:f9:3c:d2:ec:71:a8:92:25:7d:55:d3:8b:79:
    81	         5b:2d:27:c2:83:16:47:0e:2d:4a:6a:ff:e5:c3:28:92:20:a3:
    82	         d9:64:3a:c6:6e:01:60:d1:a1:44:90:7d:2e:e9:38:3c:80:4c:
    83	         ef:fc:66:bd:ed:88:6e:a9:43:5f:91:ab:89:7a:a3:c9:2b:60:
    84	         9f:2b:80:7d:82:b5:9a:c2:e8:95:61:6a:eb:f6:2d:1d:19:0e:
    85	         8b:ef:ec:b2:56:9d:e8:23:2f:8b:77:22:52:4c:ba:38:02:10:
    86	         12:d5:75:40:c6:a1:9c:3e:e6:d6:f8:8e:74:e7:e4:6c:96:38:
    87	         1f:9f:5d:a0:ca:a0:46:8b:77:43:1c:dd:70:57:78:0b:fd:10:
    88	         57:af:57:4e:94:49:4d:85:6d:8f:bb:de:21:01:d0:ef:b0:00:
    89	         c2:9f:b8:d3:64:2b:b1:5f
```

Now we have to answer the following questions:

1. What part of the certificate indicates this is a CA's certificate?

	**A:** in line 59 we have a field `CA` that is set to `TRUE`
	indicating that this is a CA certificate.

2. What part of the certificate indicates this is a self-signed
certificate?

	**A:** since the `Issuer` field in line 7 and `Subject` field
	in line 11 are the same, it means this is a self-signed
	certificate.

Our key is shown below:

```bash
[04/08/22]seed@vm:~/.../lab04$ openssl rsa -in ca.key -text -noout | cat -n
Enter pass phrase for ca.key:
     1	RSA Private-Key: (4096 bit, 2 primes)
     2	modulus:
     3	    00:dd:bb:68:b3:57:0b:9f:dd:d5:8c:3d:a2:1b:83:
     4	    de:7b:bf:25:c0:29:ce:15:5d:fb:5e:a1:7e:e2:80:
     5	    37:7e:61:28:1e:2b:c3:fe:9a:58:36:14:36:2c:2e:
     6	    03:e2:51:78:3b:4c:3d:bc:eb:cc:b6:8b:29:3f:92:
     7	    82:65:b1:9c:d0:f8:07:99:03:c8:35:8a:39:08:1d:
     8	    02:ef:38:ba:a0:6b:3e:56:d8:90:8a:3a:23:28:b1:
     9	    1e:83:9e:e1:90:36:dc:9d:fa:b0:aa:4b:2c:e3:6c:
    10	    54:ee:24:73:29:a5:c4:1e:36:d0:5c:78:99:be:a8:
    11	    ac:f3:e0:ed:de:f5:8a:ed:39:95:fc:ce:f6:34:57:
    12	    e3:c8:41:26:4e:1b:c4:e6:c7:03:d8:71:30:0f:9f:
    13	    e2:94:84:ef:7e:5c:dc:f0:3c:ab:79:61:b2:ae:f3:
    14	    63:cc:13:24:56:a8:c5:e5:24:42:bd:75:98:0c:21:
    15	    ba:83:b7:e8:a9:1c:bd:9e:93:69:db:8a:b9:5b:ea:
    16	    b6:66:3b:24:5a:56:e5:49:6a:99:7a:b8:8a:2c:7e:
    17	    d4:57:80:d6:c4:fc:f3:82:bb:fb:e6:81:4d:b2:db:
    18	    1c:31:37:9b:d6:f1:ba:27:5f:f0:c2:44:cb:79:82:
    19	    c2:44:41:83:a4:b2:3f:24:2f:32:08:0f:15:d7:62:
    20	    15:08:33:9d:61:a0:b8:25:31:4d:1e:05:0d:44:68:
    21	    03:7e:6d:c9:aa:01:a3:2f:b6:43:0b:a6:00:b9:22:
    22	    d4:64:10:98:ff:11:bc:cb:5b:08:e0:44:9f:ce:ce:
    23	    ed:87:23:86:2f:54:43:2e:a1:e1:7d:82:bb:3e:e7:
    24	    5f:0c:69:c6:56:6d:2b:83:d3:51:59:5a:96:76:5e:
    25	    bb:58:f8:3a:70:c7:e5:9d:f2:41:80:56:c1:52:2e:
    26	    da:14:53:e3:9a:ec:44:b5:fe:ed:e1:14:25:fd:07:
    27	    06:80:67:9b:9b:f4:0c:52:0e:ea:ca:a9:b3:7b:e2:
    28	    9b:f1:89:d4:02:4b:cf:c9:9c:4c:70:89:a8:a5:0c:
    29	    d6:5a:92:b6:52:6e:71:f2:69:46:aa:7e:53:a5:f1:
    30	    09:0e:88:6c:97:5a:72:11:c2:1f:8f:4c:d7:89:fa:
    31	    c9:11:4e:85:b6:be:e5:28:3b:72:8e:9e:36:dc:43:
    32	    48:8f:36:13:00:da:3e:11:51:e0:ea:d5:9f:7c:5d:
    33	    6f:51:f1:81:2e:21:78:6e:74:af:74:c8:3f:8a:47:
    34	    ca:7a:dc:97:df:bd:3f:f7:21:ab:5c:31:1e:ff:be:
    35	    31:85:40:60:4d:6d:6b:f1:45:ff:35:00:0c:0e:9d:
    36	    b0:78:c6:bb:7c:f3:33:07:4f:fd:b6:5c:2f:cc:31:
    37	    b6:b7:49
    38	publicExponent: 65537 (0x10001)
    39	privateExponent:
    40	    0c:86:67:95:a9:d7:18:42:b9:ab:60:57:e5:e1:ff:
    41	    11:52:6c:d9:7c:03:0e:25:ee:eb:1b:88:45:57:24:
    42	    cb:58:c5:a1:5e:ab:85:98:46:5f:57:a8:ef:b9:f1:
    43	    b2:39:13:e7:95:98:ef:31:0a:b1:4d:0f:19:6e:37:
    44	    3b:73:5a:fe:5a:e4:62:fa:59:b6:dc:85:f8:35:5b:
    45	    53:14:0b:46:c0:88:16:f3:c8:c6:66:1e:52:50:1e:
    46	    ff:24:5c:6d:7f:0b:db:48:ab:ee:12:ad:08:73:b8:
    47	    20:73:a2:a5:b9:9d:68:6f:84:39:94:fe:6a:4c:b0:
    48	    e8:5d:ce:52:07:e0:bf:21:bf:9d:36:3b:a6:1d:48:
    49	    da:a9:03:76:ea:0e:a7:52:9a:9f:01:55:cb:ac:26:
    50	    6a:69:4f:f5:fd:5c:e5:9d:27:ba:85:90:48:5d:ab:
    51	    47:7d:d5:51:a7:da:f5:e4:a4:78:7b:4a:09:75:66:
    52	    49:c6:7b:b8:01:cd:9c:45:8b:77:41:50:d3:26:61:
    53	    03:49:57:f6:5a:6e:3d:45:6e:42:06:66:97:b8:da:
    54	    59:cf:07:52:bd:4f:2c:15:5e:57:58:fa:80:9d:eb:
    55	    eb:c3:ce:63:c7:e7:d2:0d:93:56:05:f8:d0:76:58:
    56	    6e:ee:9c:77:13:32:89:e2:b8:9a:c7:ef:5d:e9:46:
    57	    72:c3:35:9e:4d:ac:8b:d0:ff:98:75:29:07:08:36:
    58	    01:e1:a8:c4:52:f3:7f:24:02:28:f4:4c:57:c5:78:
    59	    68:1b:0e:fd:9d:76:20:4b:91:6e:40:b9:6e:55:27:
    60	    bf:ee:64:70:98:f8:e6:91:37:c7:3f:21:2b:2f:f5:
    61	    70:66:5a:39:c0:4a:b7:34:b1:9f:1e:79:d9:64:4e:
    62	    20:6f:87:a1:e8:2a:2b:4f:57:96:07:f4:e5:6a:71:
    63	    e5:6b:77:53:c5:5a:58:e4:77:83:9f:b7:59:c5:95:
    64	    3a:ce:01:96:6b:3e:16:47:f1:44:ff:68:17:bf:9e:
    65	    a3:25:f5:8d:de:e2:d4:a6:fe:37:66:4f:db:36:56:
    66	    18:9a:9e:1e:aa:45:36:61:42:2e:2a:1d:05:85:8e:
    67	    e9:9d:e9:13:02:1d:33:4e:01:be:22:fd:db:32:04:
    68	    94:4e:68:5a:52:e7:c6:31:a9:96:c0:80:1c:b4:c7:
    69	    29:88:c2:2e:94:f7:ff:f9:53:fc:c5:0b:9e:fa:65:
    70	    8d:f7:e3:31:5c:b3:ab:a0:04:03:18:bd:56:80:48:
    71	    a3:ea:eb:b1:d2:6c:7b:0f:d9:1d:bb:9f:fd:b0:f0:
    72	    63:6d:65:21:ec:d3:68:31:3a:87:33:0c:56:16:b2:
    73	    8b:1d:04:fa:c6:24:fc:98:c1:f6:42:ee:66:6e:fd:
    74	    da:59
    75	prime1:
    76	    00:f9:84:f7:4a:b2:c3:d5:4b:28:bc:41:2f:48:d4:
    77	    86:33:66:74:89:66:4a:a2:f0:94:a2:b9:b4:dd:6d:
    78	    3a:05:d4:0b:0a:bf:8e:1b:4d:1e:40:c8:d1:ba:9d:
    79	    01:57:c1:20:10:81:e6:21:f5:b5:77:a2:7b:3b:5d:
    80	    70:43:21:92:2b:65:2e:51:85:83:50:76:57:74:df:
    81	    8c:9b:09:e0:89:eb:3f:82:f8:31:b6:74:06:f2:7f:
    82	    98:5f:26:c5:b7:0a:31:59:94:32:a8:99:fe:bc:e2:
    83	    76:1b:36:b2:39:c6:78:8a:2d:db:17:82:cf:43:99:
    84	    c4:69:20:28:8a:9b:f8:cb:40:4d:e3:19:f9:a1:f6:
    85	    fc:c5:b7:ae:e5:2b:b8:03:4b:7f:29:5d:c4:ce:a0:
    86	    41:25:9c:a7:f8:bc:9b:8e:a0:ed:5e:ef:12:ec:be:
    87	    19:c5:dd:35:35:10:81:d7:bf:6d:4f:bc:81:d1:f7:
    88	    25:dd:d8:ee:c9:22:d6:00:70:77:dc:28:a0:e8:c2:
    89	    5e:a8:8c:58:8e:85:ae:e9:31:1d:66:ed:af:ee:a4:
    90	    d1:4b:20:d4:b6:52:5a:13:62:37:eb:33:33:c1:f1:
    91	    3b:2a:de:f7:19:53:45:b9:47:73:c6:5b:9c:db:a1:
    92	    1e:8a:cc:d7:69:9d:74:59:65:29:6d:ac:19:b2:e4:
    93	    bd:bb
    94	prime2:
    95	    00:e3:7d:af:f3:a0:de:6a:d8:c8:01:be:a7:5c:84:
    96	    b6:24:5a:38:64:bf:dc:ac:55:94:4d:2e:1e:02:2c:
    97	    d4:54:af:9b:9e:b9:e6:f7:f9:09:ec:12:cd:0c:26:
    98	    20:68:db:65:77:ee:20:1b:59:b3:5f:92:4f:b2:0c:
    99	    5f:73:38:77:bd:3f:62:68:fb:90:53:e9:a3:65:cf:
   100	    da:5b:3f:ad:70:2d:4d:0a:d0:e3:55:8d:c9:6a:0a:
   101	    8d:d7:5d:98:2b:43:36:dc:eb:95:da:f3:c1:a5:e7:
   102	    b6:8a:0c:ec:28:e1:79:65:3c:63:02:00:9a:62:8b:
   103	    0b:33:bd:67:c3:17:14:98:f9:6f:ed:01:5b:30:24:
   104	    2d:34:f2:ee:bf:15:39:42:65:fa:c4:59:e5:ef:58:
   105	    66:b5:31:83:8f:9c:78:0d:cd:bb:76:f7:aa:5f:cb:
   106	    bd:60:c7:fd:18:46:d0:0e:cb:db:98:69:1e:c5:b1:
   107	    3b:fc:8d:19:2d:d5:52:bc:e7:62:2b:16:98:a7:65:
   108	    6c:10:08:86:78:bc:c9:86:43:58:d9:79:bc:a3:9d:
   109	    24:8c:ed:84:96:77:66:43:cc:01:81:f6:c5:24:e8:
   110	    2a:26:9d:bc:89:aa:82:91:5e:cb:c4:06:63:fe:97:
   111	    df:09:2b:19:7f:16:f9:9a:2b:a2:95:66:b2:89:96:
   112	    8c:cb
   113	exponent1:
   114	    79:f5:9c:ed:bc:fc:ed:67:44:2c:e7:eb:95:bc:93:
   115	    2f:d2:77:49:a5:5e:fb:cf:7d:c7:1f:e0:20:8e:bf:
   116	    da:93:6f:26:3c:c4:d7:e1:54:d5:33:93:17:94:a0:
   117	    50:65:4c:27:e5:66:37:d9:22:6e:ac:53:71:8b:d5:
   118	    50:6a:bb:48:4c:b7:5c:0f:57:44:df:e3:15:74:23:
   119	    04:b0:ca:38:7f:6c:d6:b6:3b:8e:43:9e:15:af:7a:
   120	    29:a0:08:e9:d3:a8:95:71:53:65:87:87:b4:89:38:
   121	    53:4a:e3:c8:4b:93:25:6f:0e:74:aa:72:66:47:65:
   122	    72:40:ce:b0:19:56:7c:fa:1c:57:d1:9d:6b:a6:0e:
   123	    9d:11:90:01:e1:e1:9a:72:af:5f:e7:be:47:84:9a:
   124	    f3:28:64:0d:b7:1a:8d:97:69:0e:8a:ea:84:1e:d2:
   125	    f8:1c:8c:62:38:84:4e:90:f9:7c:0c:d6:a3:6b:1a:
   126	    ee:19:97:75:66:7a:95:52:63:b3:8f:84:3e:e0:aa:
   127	    65:d5:db:7f:26:26:7d:42:8d:6d:7e:e6:0c:4e:31:
   128	    22:78:94:9f:dd:16:bb:fc:b8:fa:54:57:8b:07:3e:
   129	    f5:50:70:14:c7:d7:ff:9c:c6:21:01:2c:65:49:96:
   130	    65:1f:84:76:d2:08:49:9d:a1:a7:42:84:79:25:46:
   131	    fb
   132	exponent2:
   133	    37:b1:11:5e:27:47:cc:08:7e:0e:4f:4a:51:8f:fb:
   134	    b7:32:8f:13:bb:14:9e:eb:e4:7e:70:87:d0:28:81:
   135	    62:aa:0b:02:f4:a7:e2:95:09:24:ce:05:7c:af:e8:
   136	    c7:42:ce:b8:69:87:5a:aa:bf:0a:ea:9a:b0:0f:ef:
   137	    5e:19:b8:d6:8f:a9:db:e1:9a:17:b0:de:db:91:e2:
   138	    f6:eb:c3:dd:ee:79:29:e8:b1:7e:c4:53:46:ae:86:
   139	    c2:ee:9a:ce:1c:8a:55:d3:26:c0:8f:64:1c:e5:78:
   140	    fe:9a:e3:45:ae:cd:35:47:6f:45:92:38:39:5f:a8:
   141	    c2:65:df:fd:16:ad:68:35:c8:7c:d1:14:2d:d8:ed:
   142	    68:45:00:c5:ae:c0:71:cc:dd:73:2c:3e:ea:35:4e:
   143	    0f:95:ab:8b:80:d6:b3:2a:66:76:5d:73:4c:38:68:
   144	    88:44:ed:65:00:10:f7:4e:58:d8:1b:03:f9:89:c6:
   145	    11:b1:c9:34:3b:41:22:15:d3:26:91:45:95:d5:72:
   146	    7b:0a:5b:41:3b:96:6a:54:1a:70:43:29:37:34:c5:
   147	    39:ea:fb:c6:8a:3c:72:e4:82:89:fa:8b:90:d9:75:
   148	    58:36:cd:6d:36:5c:f8:91:a8:78:b8:c7:ca:10:0b:
   149	    fe:06:9b:1b:4d:d8:95:31:91:7f:d9:c0:63:86:83:
   150	    b9
   151	coefficient:
   152	    00:97:22:cc:ec:05:a8:92:da:35:28:69:27:ba:1f:
   153	    5d:b9:f5:b8:e3:76:17:f5:01:93:08:5b:62:82:73:
   154	    1f:43:de:35:1f:8d:de:b6:04:d8:88:33:97:5b:80:
   155	    fc:30:cd:c0:47:52:ac:81:a6:0c:85:60:09:d4:44:
   156	    3f:b3:f2:54:12:41:02:ef:c3:d3:a0:e5:b1:31:07:
   157	    ef:0d:71:e7:b4:08:f2:ea:73:8f:0e:88:e3:a5:78:
   158	    58:64:86:3a:5f:de:d4:0f:cc:9a:fa:7c:78:94:66:
   159	    05:ca:43:b6:70:bd:ee:97:2a:ba:c8:01:c3:25:8e:
   160	    a3:e0:0f:b7:7a:85:2c:67:42:86:fe:6d:a9:98:18:
   161	    e8:d5:cf:81:b4:4f:bb:a0:97:15:e7:81:a1:e3:18:
   162	    c6:77:2a:dc:21:87:51:ff:16:eb:c2:98:24:97:8a:
   163	    db:53:45:70:23:d4:ee:ad:e6:2a:8c:49:60:cd:09:
   164	    3b:c3:c2:d6:ef:64:1b:5c:72:ca:19:f6:0f:a5:4f:
   165	    1a:18:eb:d1:ff:48:d1:e1:57:7f:e3:30:1b:a8:43:
   166	    ff:c0:fd:7d:ac:66:0e:4d:16:4d:57:7b:2f:1d:4a:
   167	    85:41:48:ba:e1:90:39:06:4c:c9:f0:15:a7:2c:db:
   168	    13:83:ac:84:1c:79:8d:d3:0d:4c:f8:62:37:b3:31:
   169	    e2:df
```

3. In the RSA algorithm, we have a public exponent e, a private
   exponent d, a modulus n, and two secret numbers p and q, such that n
   = pq. Please identify the values for these elements in your
   certificate and key files.

	**A:** The public exponent *e* is `65537` (line 38), the private
	exponent *d* is shown in line 39 (too big to put here), the modulus *n*
	is shown in line 2, and *p* and *q* are the `prime1` and `prime2` shown
	in lines 75 and 94.
		

## Task 2: Generating a Certificate Request for Your Web Server

```bash
[04/08/22]seed@vm:~/.../lab04$ openssl req -newkey rsa:2048 -sha256 -keyout serverbank.key -out serverbank.csr -subj "/CN=www.bankG7.com/O=BankG7 Inc./C=US" -passout pass:dees -addext "subjectAltName = DNS:www.bankG7.com, DNS:www.bG7.com, DNS:www.G7bank.com"
Generating a RSA private key
...................+++++
........................................................................+++++
writing new private key to 'serverbank.key'
-----

```

The CSR generated is shown below:

```
     1	Certificate Request:
     2	    Data:
     3	        Version: 1 (0x0)
     4	        Subject: CN = www.bankG7.com, O = BankG7 Inc., C = US
     5	        Subject Public Key Info:
     6	            Public Key Algorithm: rsaEncryption
     7	                RSA Public-Key: (2048 bit)
     8	                Modulus:
     9	                    00:a4:3d:0f:03:28:3f:ed:4b:f6:2f:42:10:42:24:
    10	                    0c:ab:f2:7e:85:66:46:53:28:e4:98:7a:20:f5:ce:
    11	                    b2:b2:1f:9b:c4:bd:ed:08:33:91:96:db:d6:9c:70:
    12	                    87:ac:42:35:4d:58:a9:d4:32:8d:b6:81:61:2a:ef:
    13	                    4a:8c:d2:2c:62:61:89:52:87:8b:d7:5c:c3:a4:4c:
    14	                    9e:33:60:78:46:63:08:58:5d:c3:2d:9a:48:9d:49:
    15	                    8b:6e:ff:6c:a7:17:0e:64:1e:3a:57:73:57:f5:9c:
    16	                    70:c7:84:31:83:16:3c:62:82:4d:06:92:1f:30:7d:
    17	                    39:95:03:b6:bb:03:47:e3:34:31:f2:8a:59:b9:21:
    18	                    a0:f4:33:04:25:47:8d:11:9c:01:f8:fa:36:b9:9d:
    19	                    d7:f5:89:a7:eb:eb:ed:51:b6:5c:1f:cd:53:04:64:
    20	                    d1:91:17:18:ad:4e:9c:26:81:a0:5c:ef:b0:eb:49:
    21	                    12:0d:af:09:d3:13:86:ab:d7:5b:a5:ac:a1:d4:7c:
    22	                    55:cc:1a:79:70:12:d7:72:4e:2b:02:ff:75:a1:e1:
    23	                    f7:cc:ed:86:c4:82:89:c2:1f:43:42:c4:c5:9e:61:
    24	                    a3:5d:51:98:84:76:98:44:fe:7e:10:fa:b6:26:fc:
    25	                    84:97:99:8b:9b:05:aa:9f:7c:99:4b:c1:2f:8c:bf:
    26	                    a0:db
    27	                Exponent: 65537 (0x10001)
    28	        Attributes:
    29	        Requested Extensions:
    30	            X509v3 Subject Alternative Name: 
    31	                DNS:www.bankG7.com, DNS:www.bG7.com, DNS:www.G7bank.com
    32	    Signature Algorithm: sha256WithRSAEncryption
    33	         9d:d1:89:ad:86:c0:ec:11:a0:26:df:d3:ff:27:b2:84:27:f1:
    34	         8b:3d:eb:0b:62:78:3c:65:e2:0f:d9:05:15:56:77:c4:3d:7e:
    35	         f9:90:99:b5:35:23:64:93:30:4e:de:4d:da:b1:cd:ae:91:62:
    36	         75:d6:c6:ad:bc:2e:a2:74:e4:2e:6a:e8:38:2d:1c:5b:d4:1c:
    37	         81:89:99:d6:db:85:8d:3e:8e:39:15:ef:61:bf:b1:e8:91:2a:
    38	         87:42:3d:38:9f:7a:4b:e8:1c:51:5c:17:00:c6:7e:ad:ab:32:
    39	         fe:2c:36:66:ab:b4:35:b1:88:00:11:df:81:cb:ca:fd:37:ba:
    40	         49:fa:fc:47:c7:d8:32:8d:1d:da:6b:24:e3:a2:95:0a:67:a4:
    41	         ec:e4:56:1c:fb:d4:ce:21:5d:7b:cf:70:4e:8a:6b:c8:9a:81:
    42	         a1:db:47:33:4b:01:ec:19:23:53:2c:02:e1:dc:f9:92:9c:11:
    43	         f5:86:73:79:dd:43:fc:64:3d:af:28:7a:66:2f:e5:cf:5d:4d:
    44	         27:33:f2:9c:3d:2e:b0:62:f7:4f:a7:b4:9d:79:ae:32:c4:97:
    45	         97:b2:a6:dd:06:bd:ed:25:c4:46:ab:d7:29:a2:2e:a2:5d:96:
    46	         09:b0:75:20:1e:68:3a:12:17:59:d5:60:34:53:ec:83:49:f3:
    47	         b9:10:ed:5f
```

As it can be seen in lines 30 and 31, we have alternative names set.

The generated key is also shown below:

```
     1	RSA Private-Key: (2048 bit, 2 primes)
     2	modulus:
     3	    00:a4:3d:0f:03:28:3f:ed:4b:f6:2f:42:10:42:24:
     4	    0c:ab:f2:7e:85:66:46:53:28:e4:98:7a:20:f5:ce:
     5	    b2:b2:1f:9b:c4:bd:ed:08:33:91:96:db:d6:9c:70:
     6	    87:ac:42:35:4d:58:a9:d4:32:8d:b6:81:61:2a:ef:
     7	    4a:8c:d2:2c:62:61:89:52:87:8b:d7:5c:c3:a4:4c:
     8	    9e:33:60:78:46:63:08:58:5d:c3:2d:9a:48:9d:49:
     9	    8b:6e:ff:6c:a7:17:0e:64:1e:3a:57:73:57:f5:9c:
    10	    70:c7:84:31:83:16:3c:62:82:4d:06:92:1f:30:7d:
    11	    39:95:03:b6:bb:03:47:e3:34:31:f2:8a:59:b9:21:
    12	    a0:f4:33:04:25:47:8d:11:9c:01:f8:fa:36:b9:9d:
    13	    d7:f5:89:a7:eb:eb:ed:51:b6:5c:1f:cd:53:04:64:
    14	    d1:91:17:18:ad:4e:9c:26:81:a0:5c:ef:b0:eb:49:
    15	    12:0d:af:09:d3:13:86:ab:d7:5b:a5:ac:a1:d4:7c:
    16	    55:cc:1a:79:70:12:d7:72:4e:2b:02:ff:75:a1:e1:
    17	    f7:cc:ed:86:c4:82:89:c2:1f:43:42:c4:c5:9e:61:
    18	    a3:5d:51:98:84:76:98:44:fe:7e:10:fa:b6:26:fc:
    19	    84:97:99:8b:9b:05:aa:9f:7c:99:4b:c1:2f:8c:bf:
    20	    a0:db
    21	publicExponent: 65537 (0x10001)
    22	privateExponent:
    23	    79:59:a0:08:fe:29:55:c4:e5:8e:7f:6a:91:e4:1e:
    24	    f4:c1:44:98:7e:3c:01:df:da:1f:8b:45:65:39:a7:
    25	    31:13:47:bd:b3:73:3a:b7:17:a0:76:a8:78:88:70:
    26	    c6:67:59:08:e0:35:03:53:a2:ff:b1:52:c1:2d:73:
    27	    50:df:4d:ea:17:15:26:c4:7e:1f:cf:2b:c2:ee:d5:
    28	    89:32:83:4b:c6:8a:78:f8:36:69:30:70:ec:be:55:
    29	    66:db:15:45:d4:34:4e:e7:42:c6:30:b4:52:06:98:
    30	    11:e2:eb:ce:66:94:91:58:a3:5e:f2:82:57:0a:7f:
    31	    8c:49:e5:2f:02:ba:8f:f5:66:db:1b:73:89:f5:ac:
    32	    ee:e6:f8:0a:73:b2:7b:0a:ed:da:1b:96:a1:3c:59:
    33	    db:27:4b:eb:ff:01:b5:1b:46:8d:6a:a3:fd:96:a5:
    34	    99:25:69:60:63:4e:80:cb:45:f4:15:d3:d7:a2:51:
    35	    2b:ed:98:21:5a:52:16:58:a1:c7:bb:ec:0a:04:3c:
    36	    d8:b5:6f:f2:02:a5:dd:b2:24:72:24:5d:fa:55:01:
    37	    36:8c:c5:d1:ac:d0:a0:3a:8d:4f:99:23:a8:4f:c8:
    38	    99:76:23:d2:57:13:5f:98:ad:31:7a:db:d5:4e:bb:
    39	    6a:c6:70:d2:fa:3f:e1:7d:dc:76:61:49:6d:e8:93:
    40	    91
    41	prime1:
    42	    00:cd:82:ce:82:22:79:e2:f4:27:76:ee:2c:51:ea:
    43	    66:f3:17:2f:ba:0b:03:f8:ea:11:11:50:40:68:4e:
    44	    e0:ad:a2:dc:7c:f2:48:8c:22:d8:68:57:0b:e2:69:
    45	    80:50:12:5d:f9:54:83:74:ed:d4:81:ae:af:d1:ed:
    46	    28:43:9f:f4:33:9a:ca:a7:7a:db:29:4d:d8:ca:85:
    47	    64:19:f8:5a:54:a3:9c:43:50:a7:25:09:39:34:e3:
    48	    0e:5b:4a:b7:9f:57:60:a3:4b:f2:1d:64:9a:8a:4a:
    49	    68:3b:90:20:ba:6d:e1:78:20:d3:6e:d1:46:f5:05:
    50	    e8:a1:10:1e:8a:bb:11:37:b9
    51	prime2:
    52	    00:cc:96:81:4f:26:bd:30:ea:42:4b:d0:e6:2d:cf:
    53	    8f:bb:4c:62:a0:03:a7:80:00:99:c7:31:14:3c:55:
    54	    67:34:d0:b5:4c:cf:4f:1e:3d:b9:1c:76:66:83:18:
    55	    0f:55:dd:ed:82:e2:09:d8:73:72:e9:4c:02:e0:82:
    56	    a2:21:4c:74:0e:ed:45:d0:f3:7c:44:06:51:f7:e7:
    57	    4e:17:4c:b1:7d:18:5f:7f:04:86:64:47:05:b6:65:
    58	    4b:a7:a4:f3:b2:a1:fb:2c:bb:f8:e7:f3:14:d7:4e:
    59	    19:f4:24:73:13:3e:2f:95:77:9d:7f:1b:d4:f3:92:
    60	    8e:fe:bd:5a:29:df:f3:3f:33
    61	exponent1:
    62	    00:ae:c5:4d:45:d2:78:01:90:4d:77:90:f9:53:f8:
    63	    99:f0:a5:89:4b:18:4f:1a:82:44:5d:8a:f8:1b:3c:
    64	    e9:4a:01:34:ad:75:86:48:d6:e1:5f:6a:97:8a:c6:
    65	    d9:fe:3e:80:78:a7:6c:dc:25:de:11:23:34:16:bd:
    66	    2e:fe:c3:aa:f3:af:f2:24:18:11:05:c1:54:5a:76:
    67	    b2:38:7b:c2:a6:34:35:91:f1:a7:13:92:12:7a:6d:
    68	    96:2a:01:12:c3:89:95:7f:89:f8:af:12:7d:72:99:
    69	    f0:a6:a6:dd:8e:6e:1f:ce:ff:6b:8f:d4:9e:19:86:
    70	    1a:dd:ce:4d:8b:01:a7:94:d1
    71	exponent2:
    72	    00:89:76:fb:2d:b6:c6:da:4e:1c:fb:9a:42:7f:37:
    73	    7e:c9:07:bb:20:ef:33:a5:c3:41:90:60:be:cb:07:
    74	    4b:50:66:87:db:33:54:62:ea:70:2d:da:b4:68:93:
    75	    4b:02:94:1a:99:9d:cd:a5:bd:74:d1:a2:fc:2f:e4:
    76	    36:92:0f:2f:d0:33:c7:7c:6d:8d:ea:9b:20:8a:64:
    77	    c5:c8:cc:04:5a:1c:7d:55:f8:31:60:c1:83:ea:19:
    78	    47:eb:3d:df:6d:00:7f:a7:80:29:ef:98:1a:41:cc:
    79	    bd:8d:98:cc:25:7a:dc:38:1e:e6:19:59:17:0e:0e:
    80	    44:bc:8f:78:3c:b8:b8:55:97
    81	coefficient:
    82	    29:ac:43:ac:9d:f1:be:47:b8:ae:0f:51:fa:6d:aa:
    83	    54:51:f8:5f:df:ce:f6:e8:52:e7:9a:cd:94:02:3c:
    84	    da:fe:84:9d:b9:c6:51:e0:56:37:ef:80:72:f7:31:
    85	    97:1b:2b:73:7a:ed:88:9e:7f:39:f0:29:f1:5a:51:
    86	    48:36:69:1d:de:2d:ec:2c:c9:6b:8b:fc:c1:e2:c0:
    87	    39:f0:26:f5:be:21:50:3f:69:46:03:a8:07:f4:a6:
    88	    54:75:dd:0c:89:59:b2:8b:1b:ce:e6:2c:19:db:1e:
    89	    ed:2c:c2:e8:b6:0f:7c:41:25:8a:80:93:a2:23:d8:
    90	    64:46:75:30:95:fa:8a:23
```

## Task 3: Generating a Certificate for your Server
An additional step was necessary to generate the certificate from the server
CSR. We had to convert the CA certificate and private key to PEM format. This
was done with the following commands:

```bash
[04/08/22]seed@vm:~/.../lab04$ openssl rsa -in ca.key -text > cakey.pem
Enter pass phrase for ca.key:
writing RSA key
[04/08/22]seed@vm:~/.../lab04$ openssl x509 -in ca.crt -text -noout > cacert.pem
```

After that, we were able to generate the server certificate:

```bash
[04/08/22]seed@vm:~/.../lab04$ openssl ca -config ../openssl.cnf -policy policy_anything -md sha256 -days 3650 -in serverbank.csr -out serverbank.crt -batch -cert ca.crt -key ca.key 
Using configuration from ../openssl.cnf
Check that the request matches the signature
Signature ok
Certificate Details:
        Serial Number: 8192 (0x2000)
        Validity
            Not Before: Apr  8 10:58:09 2022 GMT
            Not After : Apr  5 10:58:09 2032 GMT
        Subject:
            countryName               = US
            organizationName          = BankG7 Inc.
            commonName                = www.bankG7.com
        X509v3 extensions:
            X509v3 Basic Constraints: 
                CA:FALSE
            Netscape Comment: 
                OpenSSL Generated Certificate
            X509v3 Subject Key Identifier: 
                B1:A1:31:CA:FF:8B:29:8C:42:D2:35:C0:AD:FE:F1:76:D3:00:07:4C
            X509v3 Authority Key Identifier: 
                keyid:A9:66:A7:C6:32:B2:EB:21:5C:11:AD:74:A7:FE:86:D2:9A:7E:BE:C5

            X509v3 Subject Alternative Name: 
                DNS:www.bankG7.com, DNS:www.bG7.com, DNS:www.G7bank.com
Certificate is to be certified until Apr  5 10:58:09 2032 GMT (3650 days)

Write out database with 1 new entries
Data Base Updated
[04/08/22]seed@vm:~/.../lab04$ openssl x509 -in serverbank.crt -text -noout | cat -n
     1	Certificate:
     2	    Data:
     3	        Version: 3 (0x2)
     4	        Serial Number: 8192 (0x2000)
     5	        Signature Algorithm: sha256WithRSAEncryption
     6	        Issuer: CN = www.CAgroup7.com, O = Group7 CA LTD., C = US
     7	        Validity
     8	            Not Before: Apr  8 10:58:09 2022 GMT
     9	            Not After : Apr  5 10:58:09 2032 GMT
    10	        Subject: C = US, O = BankG7 Inc., CN = www.bankG7.com
    11	        Subject Public Key Info:
    12	            Public Key Algorithm: rsaEncryption
    13	                RSA Public-Key: (2048 bit)
    14	                Modulus:
    15	                    00:a4:3d:0f:03:28:3f:ed:4b:f6:2f:42:10:42:24:
    16	                    0c:ab:f2:7e:85:66:46:53:28:e4:98:7a:20:f5:ce:
    17	                    b2:b2:1f:9b:c4:bd:ed:08:33:91:96:db:d6:9c:70:
    18	                    87:ac:42:35:4d:58:a9:d4:32:8d:b6:81:61:2a:ef:
    19	                    4a:8c:d2:2c:62:61:89:52:87:8b:d7:5c:c3:a4:4c:
    20	                    9e:33:60:78:46:63:08:58:5d:c3:2d:9a:48:9d:49:
    21	                    8b:6e:ff:6c:a7:17:0e:64:1e:3a:57:73:57:f5:9c:
    22	                    70:c7:84:31:83:16:3c:62:82:4d:06:92:1f:30:7d:
    23	                    39:95:03:b6:bb:03:47:e3:34:31:f2:8a:59:b9:21:
    24	                    a0:f4:33:04:25:47:8d:11:9c:01:f8:fa:36:b9:9d:
    25	                    d7:f5:89:a7:eb:eb:ed:51:b6:5c:1f:cd:53:04:64:
    26	                    d1:91:17:18:ad:4e:9c:26:81:a0:5c:ef:b0:eb:49:
    27	                    12:0d:af:09:d3:13:86:ab:d7:5b:a5:ac:a1:d4:7c:
    28	                    55:cc:1a:79:70:12:d7:72:4e:2b:02:ff:75:a1:e1:
    29	                    f7:cc:ed:86:c4:82:89:c2:1f:43:42:c4:c5:9e:61:
    30	                    a3:5d:51:98:84:76:98:44:fe:7e:10:fa:b6:26:fc:
    31	                    84:97:99:8b:9b:05:aa:9f:7c:99:4b:c1:2f:8c:bf:
    32	                    a0:db
    33	                Exponent: 65537 (0x10001)
    34	        X509v3 extensions:
    35	            X509v3 Basic Constraints: 
    36	                CA:FALSE
    37	            Netscape Comment: 
    38	                OpenSSL Generated Certificate
    39	            X509v3 Subject Key Identifier: 
    40	                B1:A1:31:CA:FF:8B:29:8C:42:D2:35:C0:AD:FE:F1:76:D3:00:07:4C
    41	            X509v3 Authority Key Identifier: 
    42	                keyid:A9:66:A7:C6:32:B2:EB:21:5C:11:AD:74:A7:FE:86:D2:9A:7E:BE:C5
    43	
    44	            X509v3 Subject Alternative Name: 
    45	                DNS:www.bankG7.com, DNS:www.bG7.com, DNS:www.G7bank.com
    46	    Signature Algorithm: sha256WithRSAEncryption
    47	         6b:68:3a:3c:be:da:31:14:79:89:47:be:21:19:90:1d:47:5f:
    48	         7e:4e:64:e2:c9:59:4e:03:44:f8:67:00:b7:ff:7e:5a:cd:fd:
    49	         3c:2e:08:b0:e4:34:db:51:be:7d:5e:25:70:ab:29:16:6f:e4:
    50	         35:73:f9:37:cb:0e:b7:73:ce:cf:05:28:1a:08:93:7a:29:23:
    51	         1d:a7:23:fe:34:dc:95:22:0e:f9:b8:d6:c3:f3:5b:18:92:63:
    52	         3e:5d:f5:45:80:04:24:e1:f5:f0:ed:d7:f0:5f:00:cc:de:99:
    53	         b2:fa:35:dd:21:4e:ef:73:2f:de:11:7b:61:0f:37:73:cc:3a:
    54	         dc:91:7f:27:7c:c2:e7:83:9c:4a:1d:70:c2:ae:97:3c:88:f4:
    55	         16:b8:52:6d:ad:6e:12:d8:ec:6c:9e:73:6f:51:35:a8:a8:86:
    56	         21:76:72:30:c6:9a:58:10:a1:39:46:f2:3b:b7:3a:c8:2b:e4:
    57	         51:f1:66:96:e6:38:f5:8e:e1:dc:da:96:49:04:d1:d0:fb:62:
    58	         aa:fb:6f:91:27:37:61:d8:36:7d:c2:3a:b5:16:b1:96:6f:e2:
    59	         cb:6e:86:48:ea:c6:b3:20:36:fc:0d:d5:77:47:50:2b:7e:3a:
    60	         3e:9d:64:c7:74:37:cb:80:17:9e:3a:01:f6:74:73:21:b6:8d:
    61	         ca:f6:10:2a:66:cf:ac:12:ce:29:14:d4:1c:ee:71:e3:5a:dc:
    62	         90:0f:8e:d2:f0:ce:f4:29:6d:48:be:5f:95:6e:c2:b3:18:64:
    63	         25:da:6e:9f:c2:e2:69:ad:e9:fb:cc:a4:d3:8c:59:1f:90:63:
    64	         4e:20:3e:07:40:38:99:00:cb:23:d7:38:41:b4:55:8d:f5:85:
    65	         31:2a:d4:6e:c2:dd:a6:88:54:8a:78:2b:20:cb:e4:cd:a8:81:
    66	         86:62:a1:01:42:35:3b:50:32:e6:7b:72:29:f5:9d:90:d4:b6:
    67	         12:e6:93:15:ef:47:11:c2:96:80:ca:ff:8d:6c:01:b3:21:0b:
    68	         1e:fc:16:e2:17:b0:6a:de:12:ab:32:39:b9:de:b8:d0:f3:1c:
    69	         20:56:c5:13:78:cd:5b:57:71:51:df:93:12:db:16:a1:78:4c:
    70	         f4:6b:c7:f2:1f:f1:e8:08:78:a7:d6:0d:c2:6a:c3:5c:ac:fd:
    71	         36:d5:ad:45:68:39:25:aa:96:2f:21:10:26:3a:c2:84:ce:55:
    72	         60:64:aa:a6:3a:ad:ec:38:01:8f:48:b7:2b:d3:89:bd:5b:7b:
    73	         10:22:d5:ae:b1:ff:b5:98:c7:88:8c:f7:12:c9:10:fa:90:58:
    74	         ae:6a:10:f4:ee:d9:7d:1b:59:ff:8a:1a:7a:7b:b5:b9:84:c1:
    75	         7b:0a:3c:4b:ed:b4:be:71
```

A couple notes here. We can see the difference in the subject and issuer fields
(meaning it's not a self-signed certificate) in lines 6 and 10, the CA field is
set to false since it's only a common certificate issued by the CA (line 36)
and the alternative names are included in the certificate (lines 44 and 45).



## Task 4: Deploying Certificate is an Apache-Based HTTPS Website
Now it'sour time to set up the Bank G7 website. Because of time constraints,
our webpage will be very similar to the one provided by the seed lab - but with
a customization. The diff can be checked below.

```diff
[04/08/22]seed@vm:~/.../image_www$ diff -u index{_old,}.html 
--- index_old.html	2022-04-08 20:10:07.786327481 +0100
+++ index.html	2022-04-08 19:55:33.415237801 +0100
@@ -4,9 +4,12 @@
 <style>
 body {background-color: green}
 h1 {font-size:3cm; text-align: center; color: white;
-text-shadow: 0 0 3mm yellow}</style>
+text-shadow: 0 0 3mm yellow}
+h2 {font-size:2cm; text-align: center; color: black;
+text-shadow: 0 0 3mm silver}</style>
 </head>
 <body>
 <h1>Hello, world!</h1>
+<h2>Welcome to G7 Bank</h2>
 </body>
 </html>
```

```diff
[04/08/22]seed@vm:~/.../image_www$ diff -u index{_old,}_red.html 
--- index_old_red.html	2022-04-08 20:10:36.667595404 +0100
+++ index_red.html	2022-04-08 19:56:56.400401081 +0100
@@ -4,9 +4,12 @@
 <style>
 body {background-color: red}
 h1 {font-size:3cm; text-align: center; color: white;
-text-shadow: 0 0 3mm yellow}</style>
+text-shadow: 0 0 3mm yellow}
+h2 {font-size:2cm; text-align: center; color: black;
+	text-shadow: 0 0 3mm yellow}</style>
 </head>
 <body>
 <h1>Hello, world!</h1>
+<h2>Welcome to insecure G7 Bank</h2>
 </body>
 </html>
```

And we did have to alter the apache conf slightly to accomodate the new url,
cert and key file. The diff is also shown below.

```diff
[04/08/22]seed@vm:~/.../image_www$ diff -u bank{32,G7}_apache_ssl.conf 
--- bank32_apache_ssl.conf	2020-12-05 17:48:28.000000000 +0000
+++ bankG7_apache_ssl.conf	2022-04-08 19:53:32.080325029 +0100
@@ -1,18 +1,19 @@
 <VirtualHost *:443> 
     DocumentRoot /var/www/bank32
-    ServerName www.bank32.com
-    ServerAlias www.bank32A.com
-    ServerAlias www.bank32B.com
-    ServerAlias www.bank32W.com
+    ServerName www.bankG7.com
+    ServerAlias www.bG7.com
+    ServerAlias www.G7bank.com
     DirectoryIndex index.html
     SSLEngine On 
-    SSLCertificateFile /certs/bank32.crt
-    SSLCertificateKeyFile /certs/bank32.key
+    SSLCertificateFile /volumes/lab04/serverbank.crt
+    SSLCertificateKeyFile /volumes/lab04/serverbank.key
 </VirtualHost>
 
 <VirtualHost *:80> 
     DocumentRoot /var/www/bank32
-    ServerName www.bank32.com
+    ServerName www.bankG7.com
+    ServerAlias www.bG7.com
+    ServerAlias www.G7bank.com
     DirectoryIndex index_red.html
 </VirtualHost>

```

Small note here about the failure to access the website via https. Since we've
created a CA from thin air, firefox and other browsers don't trust it (as they
should!). This is why we get a giant warning about our certificate problem.

![](img/not_trustworthy.png)

After we add our CA certificate to firefox database, it'll trust our CA and
will allow the navigation.

![](img/riding_with_the_king.png)

Another small note: the Dockerfile in the `image_www` folder have to be
changed to copy the new apache conf file to the docker container.

## Task 5: Launching a Man-In-The-Middle Attack
PKI defeats this MitM attack in 2 fronts. Firstly, it breaks the more
simplistic approach of just changing the public key since it's embedded in the
certificate signed by the CA. If a malicious actor changes the key in the
certificate, the web browser will detect it when trying to validate the CA
signature. The other approach (a more sophisticated one) would be intercepting
the connection and changing the entire certificate for a newly generated one
from a self-created CA. Though this won't work, as seen previosly, because the
browser don't trust a new CA coming out of the blue, so it will warn the user.

The second approach was conducted in this lab. See the step by step below:

1. Create a CSR for www.facebook.com and www.fb.com.
```bash
[04/08/22]seed@vm:~/.../lab04$ openssl req -newkey rsa:2048 -sha256 -keyout serverfb.key -out serverfb.csr -subj "/CN=www.facebook.com/O=Facebook Inc./C=US" -passout pass:dees -addext "subjectAltName = DNS:www.facebook.com, DNS:www.fb.com"
Generating a RSA private key
..................+++++
..........+++++
writing new private key to 'serverfb.key'
-----
```

2. Sign the certificate with selfmade CA.
```bash
[04/08/22]seed@vm:~/.../lab04$ openssl ca -config ../openssl.cnf -policy policy_anything -md sha256 -days 3650 -in serverfb.csr -out serverfb.crt -batch -cert ca.crt -key ca.key 
Using configuration from ../openssl.cnf
Check that the request matches the signature
Signature ok
Certificate Details:
        Serial Number: 8194 (0x2002)
        Validity
            Not Before: Apr  8 20:09:44 2022 GMT
            Not After : Apr  5 20:09:44 2032 GMT
        Subject:
            countryName               = US
            organizationName          = Facebook Inc.
            commonName                = www.facebook.com
        X509v3 extensions:
            X509v3 Basic Constraints: 
                CA:FALSE
            Netscape Comment: 
                OpenSSL Generated Certificate
            X509v3 Subject Key Identifier: 
                6E:C2:1A:56:28:E6:D4:1B:67:BE:AB:1E:AC:E5:3D:8F:2C:E2:3B:61
            X509v3 Authority Key Identifier: 
                keyid:A9:66:A7:C6:32:B2:EB:21:5C:11:AD:74:A7:FE:86:D2:9A:7E:BE:C5

            X509v3 Subject Alternative Name: 
                DNS:www.facebook.com, DNS:www.fb.com
Certificate is to be certified until Apr  5 20:09:44 2032 GMT (3650 days)

Write out database with 1 new entries
Data Base Updated
```

3. Download the fb home page.
```bash
curl https://www.facebook.com > index_fb.com
```

4. Set up an appropriate apache conf.
```
<VirtualHost *:443> 
    DocumentRoot /var/www/bank32
    ServerName www.facebook.com
    ServerAlias www.fb.com
    DirectoryIndex index_fb.html
    SSLEngine On 
    SSLCertificateFile /volumes/lab04/serverfb.crt
    SSLCertificateKeyFile /volumes/lab04/serverfb.key
</VirtualHost>

<VirtualHost *:80> 
    DocumentRoot /var/www/bank32
    ServerName www.facebook.com
    ServerAlias www.fb.com
    DirectoryIndex index_fb.html
</VirtualHost>

# Set the following gloal entry to suppress an annoying warning message
ServerName localhost
```

5. Build and execute the container.

Even after this, the picture below shows the failure in tricking the browser.

![](img/soo_close.png)

## Task 6:
But imagine that someway we are able to generate a trusted certificate (an
private key of a root CA compromised, added the selfmade CA to the list of
trusted CA's of the browser, etc). Then we can really impersonate fb without
sounding any alarms in the victim's browser.

To do this task in the lab, we added our G7 CA to the browser's list of trusted
CA's. As it can be seen below, the attack was sucessful.

![](img/you_have_been_pawned.png)

## Conclusion
In this lab, we have learned a lot about how the public key infrastructure
works. We've seen the implications of using PKI to MITM attacks and realized
how to bypass it in very singular conditions.
