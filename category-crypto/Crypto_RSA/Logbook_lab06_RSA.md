# Logbook for Lab 06 RSA Public-Key Encryption and Signature Lab

## Task 1: Deriving the Private Key
Using the algorithm, we can write a `task1()` function to derive the private
key `d`:

```c
void task1() {
	BN_CTX *ctx = BN_CTX_new();
	BIGNUM *p = BN_new();
	BIGNUM *pl1 = BN_new();
	BIGNUM *q = BN_new();
	BIGNUM *ql1 = BN_new();
	BIGNUM *e = BN_new();
	BIGNUM *t = BN_new();
	BIGNUM *d = BN_new();
	BIGNUM *one = BN_new();

	// Initialize p, q, e and one
	BN_one(one);
	BN_hex2bn(&p, "F7E75FDC469067FFDC4E847C51F452DF");
	BN_hex2bn(&q, "E85CED54AF57E53E092113E62F436F4F");
	BN_hex2bn(&e, "0D88C3");

	// t = totient(n) = (p - 1)*(q - 1)
	BN_sub(pl1, p, one);
	BN_sub(ql1, q, one);
	BN_mul(t, pl1, ql1, ctx);
	// d = modinv(e, t)
	BN_mod_inverse(d, e, t, ctx);
	printBN("d = ", d);
}
```

And execute it:

```bash
 charles   main … 2  …  ses  labs  lab-6  gcc rsa.c -o rsa -Wall -lcrypto
 charles   main … 2  …  ses  labs  lab-6  ./rsa
d =  3587A24598E5F2A21DB007D89D18CC50ABA5075BA19A33890FE7C28A9B496AEB
```

## Task 2: Encrypting a Message
So, the first thing is to convert the ascii string to hex:

```bash
 charles   main … 1  …  ses  labs  lab-6  1  python3 -c 'print("A top secret!".encode("UTF-8").hex())'
4120746f702073656372657421
```

Now we create a `task2()` function to execute the encryption:

```c
void task2() {
	BN_CTX *ctx = BN_CTX_new();
	BIGNUM *n = BN_new();
	BIGNUM *e = BN_new();
	BIGNUM *plaintext = BN_new();
	BIGNUM *ciphertext = BN_new();

	// Initialize p, q, e and one
	BN_hex2bn(&n, "DCBFFE3E51F62E09CE7032E2677A78946A849DC4CDDE3A4D0CB81629242FB1A5");
	BN_hex2bn(&e, "010001");
	BN_hex2bn(&plaintext, "4120746f702073656372657421");

	// encryption: ciphertext =  plaintext^e mod n
	BN_mod_exp(ciphertext, plaintext, e, n, ctx);
	printBN("Ciphertext = ", ciphertext);
}
```

And we execute the program:

```bash
 charles   main ✚ 1 … 1  …  ses  labs  lab-6  gcc -Wall rsa.c -o rsa -lcrypto
 charles   main ✚ 1 … 1  …  ses  labs  lab-6  ./rsa
Ciphertext =  6FB078DA550B2650832661E14F4F8D2CFAEF475A0DF3A75CACDC5DE5CFC5FADC
```

## Task 3: Decrypting a Message
Now we have to write a program to decript a message with the private key:

```c
void task3() {
	BN_CTX *ctx = BN_CTX_new();
	BIGNUM *n = BN_new();
	BIGNUM *d = BN_new();
	BIGNUM *plaintext = BN_new();
	BIGNUM *ciphertext = BN_new();

	// Initialize p, q, d
	BN_hex2bn(&n, "DCBFFE3E51F62E09CE7032E2677A78946A849DC4CDDE3A4D0CB81629242FB1A5");
	BN_hex2bn(&d, "74D806F9F3A62BAE331FFE3F0A68AFE35B3D2E4794148AACBC26AA381CD7D30D");
	BN_hex2bn(&ciphertext, "8C0F971DF2F3672B28811407E2DABBE1DA0FEBBBDFC7DCB67396567EA1E2493F");

	// encryption: plaintext =  ciphertext^d mod n
	BN_mod_exp(plaintext, ciphertext, d, n, ctx);
	printBN("Plaintext = ", plaintext);
}
```

And test it:

```bash
 charles   main ✚ 2 … 1  …  ses  labs  lab-6  gcc -Wall rsa.c -o rsa -lcrypto
 charles   main ✚ 2 … 1  …  ses  labs  lab-6  ./rsa
Plaintext =  50617373776F72642069732064656573
 charles   main ✚ 2 … 1  …  ses  labs  lab-6  1  python3 -c 'print(bytes.fromhex("50617373776F72642069732064656573").decode("UTF-8"))'
Password is dees
```

## Task 4: Signing a Message
Now let's sign a message, but first let's convert it to hex:

```bash
 charles   main … 1  …  ses  labs  lab-6  1  python3 -c 'print("M = I owe you $2000.".encode("UTF-8").hex())'
4d203d2049206f776520796f752024323030302e
```

And now write a signing function:

```c
void task4() {
	BN_CTX *ctx = BN_CTX_new();
	BIGNUM *n = BN_new();
	BIGNUM *d = BN_new();
	BIGNUM *plaintext_to_sign = BN_new();
	BIGNUM *signature = BN_new();

	// Initialize p, q, d
	BN_hex2bn(&n, "DCBFFE3E51F62E09CE7032E2677A78946A849DC4CDDE3A4D0CB81629242FB1A5");
	BN_hex2bn(&d, "74D806F9F3A62BAE331FFE3F0A68AFE35B3D2E4794148AACBC26AA381CD7D30D");
	BN_hex2bn(&plaintext_to_sign, "4d203d2049206f776520796f752024323030302e");

	// encryption: signature =  plaintext_to_sign^d mod n
	BN_mod_exp(signature, plaintext_to_sign, d, n, ctx);
	printBN("Signature = ", signature);
}
```

And finally sign it:

```bash
 charles   main ✚ 2 … 1  …  ses  labs  lab-6  gcc -Wall rsa.c -o rsa -lcrypto
 charles   main ✚ 2 … 1  …  ses  labs  lab-6  ./rsa
Signature =  D866CFC4C4E4B73E745EFC99967D7D962261ABC535D03433E07065419C58DC1C
```

And trying with a slight variation in the message:

```bash
 charles   main ✚ 2 … 1  …  ses  labs  lab-6  gcc -Wall rsa.c -o rsa -lcrypto
 charles   main ✚ 2 … 1  …  ses  labs  lab-6  ./rsa
Signature =  ADA7BF3628FE1D8D6A0F0B9F436EDFD82AB44306AC243862A25068DBB2217E79
```

As we can see, a small variation in the message, generated a big variation in
the signature (even without using a hash digest). This is a nice property for
a cripto signature algortihm (well done and thanks RSA).

## Task 5: Verifying a Signature
TO verify a signature, let's first convert the string to hex:

```bash
 charles   main … 1  …  ses  labs  lab-6  1  python3 -c 'print("Launch a missile.".encode("UTF-8").hex())'
4c61756e63682061206d697373696c652e
```

And then write a verifying fuction:

```c
void task5() {
	BN_CTX *ctx = BN_CTX_new();
	BIGNUM *n = BN_new();
	BIGNUM *e = BN_new();
	BIGNUM *plaintext_to_sign = BN_new();
	BIGNUM *sign_generated = BN_new();
	BIGNUM *signature = BN_new();

	// Initialize p, q, d
	BN_hex2bn(&n, "AE1CD4DC432798D933779FBD46C6E1247F0CF1233595113AA51B450F18116115");
	BN_hex2bn(&e, "010001");
	BN_hex2bn(&plaintext_to_sign, "4C61756E63682061206D697373696C652E");
	BN_hex2bn(&signature, "643D6F34902D9C7EC90CB0B2BCA36C47FA37165C0005CAB026C0542CBDB6802F");

	// encryption: signature =  plaintext_to_sign^d mod n
	BN_mod_exp(sign_generated, signature, e, n, ctx);
	printBN("Signature = ", plaintext_to_sign);
	printBN("Signature expected = ", sign_generated);

	if (BN_cmp(plaintext_to_sign, sign_generated) == 0) {
		printf("Message verified!\n");
	} else {
		printf("Message corrupted!\n");
	}
}
```

Lastly, let's see the output:

```bash
 charles   main ✚ 1 … 1  …  ses  labs  lab-6  gcc -Wall rsa.c -o rsa -lcrypto
 charles   main ✚ 1 … 1  …  ses  labs  lab-6  ./rsa
Signature =  4C61756E63682061206D697373696C652E
Signature expected =  4C61756E63682061206D697373696C652E
Message verified!
```

Now let's corrupt one byte of the signature and test it:

```bash
 charles   main ✚ 1 … 1  …  ses  labs  lab-6  gcc -Wall rsa.c -o rsa -lcrypto
 charles   main ✚ 1 … 1  …  ses  labs  lab-6  ./rsa
Signature =  4C61756E63682061206D697373696C652E
Signature expected =  91471927C80DF1E42C154FB4638CE8BC726D3D66C83A4EB6B7BE0203B41AC294
Message corrupted!
```

As expected, changing anything in the signature (or the message itself - imagine
a malicious actor trying to alter a message) will result in a failed verification.

## Task 6: Manually Verifying an X.509 Certificate
Let's verify a real world certificate from a website using our program.
To do that, we'll have to follow the steps below.

### Step 1: Download a certificate from a real web server.
Firstly, let's download Debian's X.509 certificate:

```bash
 charles   main … 1  …  ses  labs  lab-6  openssl s_client --connect www.debian.org:443 -showcerts 
CONNECTED(00000003)
depth=2 C = US, O = Internet Security Research Group, CN = ISRG Root X1
verify return:1
depth=1 C = US, O = Let's Encrypt, CN = R3
verify return:1
depth=0 CN = www.debian.org
verify return:1
---
Certificate chain
 0 s:CN = www.debian.org
   i:C = US, O = Let's Encrypt, CN = R3
   a:PKEY: rsaEncryption, 4096 (bit); sigalg: RSA-SHA256
   v:NotBefore: Apr 25 00:55:48 2022 GMT; NotAfter: Jul 24 00:55:47 2022 GMT
-----BEGIN CERTIFICATE-----
MIIGITCCBQmgAwIBAgISA5RUamPATHNGs0R8Vw2+nJJjMA0GCSqGSIb3DQEBCwUA
MDIxCzAJBgNVBAYTAlVTMRYwFAYDVQQKEw1MZXQncyBFbmNyeXB0MQswCQYDVQQD
EwJSMzAeFw0yMjA0MjUwMDU1NDhaFw0yMjA3MjQwMDU1NDdaMBkxFzAVBgNVBAMT
Dnd3dy5kZWJpYW4ub3JnMIICIjANBgkqhkiG9w0BAQEFAAOCAg8AMIICCgKCAgEA
3IjOFCB25S2G62qNF4ooySaQ+EoCDq5LuomsMNPwH05pfyleb36W9Y3khIVlRIOx
LIcHeHSqMtkaglYX8XvIAPLgqk3Tv8FjRKGs9RyViK/jP1J6ifQihkoXXF76nxdP
TXViiuzmf3v3J+PAoSgu3nHa1w359NdjJb11WgzOYm+OplO3EtkjUYpbgTvORwul
J69bp7efCSYIWhf6s9wtoNPFxk4X89ArW56K11FTaVbu4r6NPXUHOH0zboZCIXtZ
AZw4trOway3Q7AsdhtOWWMK0VGmQV8cR9Dp3Xn1LSI1JLcMg1fU416ZLe3ohJBV0
pQr08JYOvp/XHsllJn8b09I4ej+sVyzmrzLvEBkRdUTDew3+Sm3/6Fzn9RocHldy
KMN4WBXtfixptKYGxKkX4i3d0o4aUE3SiXX9/SrbUNkzBXb4xfsD/DH+SOVukzEw
AVowjuUugaPK1MDLufB9QZIHlEpUc36dOK1hN4OupwFxteka5lvV7R/Kbp1nr44z
lpopu8b8zlMSSg7AGh3aZifrOiHiRLskHatB0O8FrIPsksnQhfzHzWazH4PVBzhD
Ekuw5GYda6M7qbaSi0W/jS5fDr9Y7b5gxvRRnFUawdpsd1u+0+rKY8zFjhcYLknT
QqVUqweSKOqrUA8/KHEQcYbxfKKD4EljeZWy4LCWDVMCAwEAAaOCAkgwggJEMA4G
A1UdDwEB/wQEAwIFoDAdBgNVHSUEFjAUBggrBgEFBQcDAQYIKwYBBQUHAwIwDAYD
VR0TAQH/BAIwADAdBgNVHQ4EFgQUxdO1W5AIZHRcY1+hrQC5ygjjuDcwHwYDVR0j
BBgwFoAUFC6zF7dYVsuuUAlA5h+vnYsUwsYwVQYIKwYBBQUHAQEESTBHMCEGCCsG
AQUFBzABhhVodHRwOi8vcjMuby5sZW5jci5vcmcwIgYIKwYBBQUHMAKGFmh0dHA6
Ly9yMy5pLmxlbmNyLm9yZy8wGQYDVR0RBBIwEIIOd3d3LmRlYmlhbi5vcmcwTAYD
VR0gBEUwQzAIBgZngQwBAgEwNwYLKwYBBAGC3xMBAQEwKDAmBggrBgEFBQcCARYa
aHR0cDovL2Nwcy5sZXRzZW5jcnlwdC5vcmcwggEDBgorBgEEAdZ5AgQCBIH0BIHx
AO8AdQDfpV6raIJPH2yt7rhfTj5a6s2iEqRqXo47EsAgRFwqcwAAAYBebppfAAAE
AwBGMEQCIEVgWneSfknPPIVEFrnxO1oLj0o0kWu6cxqoFnxF27l0AiBmM+5V+dZc
AOnZ4i/pUaAzpTGKGheUAQCMAKgo0N2yjAB2ACl5vvCeOTkh8FZzn2Old+W+V32c
YAr4+U1dJlwlXceEAAABgF5umlgAAAQDAEcwRQIhAIJDlGm6Xh2iZ2k1THQK/2tS
cP+w2eQ8gnKbavdIGvgwAiBuNIWA8E5Wx0J5Mz/lCGOJDJGNYIWBSQogH8ABItsX
5jANBgkqhkiG9w0BAQsFAAOCAQEAkNC9W2CWWhy1gyPGDrdqVGku7ghwi3V6LRhE
xWJ5BFNmaJqqSckkJFNDyxSPa7VgCSamBWTXEmYeJ3thKqOO7IGm0gLxXT1FsSBa
4Jg0zmVakJIracHPNuT3UyE14/a9wuQSiFyAkP5RM8aCEG5PyE7fEAIdrqe6SJi1
rDpA4PK5Q88KMgHgHly/vPs/JkU7IHWGb1Tj/70Y052xqgbs3bft4uTGlK452KcK
BAas6KsZ/SSjc8QuVCzEfoer05ET5OSWH0MFpQ3V73SZJsufdcqgata6XVhuCcOX
mqpvsCIpv/2Pa7e2vSpnYjej8aYIaFf9erwyZiqYxJBf2TKfgA==
-----END CERTIFICATE-----
 1 s:C = US, O = Let's Encrypt, CN = R3
   i:C = US, O = Internet Security Research Group, CN = ISRG Root X1
   a:PKEY: rsaEncryption, 2048 (bit); sigalg: RSA-SHA256
   v:NotBefore: Sep  4 00:00:00 2020 GMT; NotAfter: Sep 15 16:00:00 2025 GMT
-----BEGIN CERTIFICATE-----
MIIFFjCCAv6gAwIBAgIRAJErCErPDBinU/bWLiWnX1owDQYJKoZIhvcNAQELBQAw
TzELMAkGA1UEBhMCVVMxKTAnBgNVBAoTIEludGVybmV0IFNlY3VyaXR5IFJlc2Vh
cmNoIEdyb3VwMRUwEwYDVQQDEwxJU1JHIFJvb3QgWDEwHhcNMjAwOTA0MDAwMDAw
WhcNMjUwOTE1MTYwMDAwWjAyMQswCQYDVQQGEwJVUzEWMBQGA1UEChMNTGV0J3Mg
RW5jcnlwdDELMAkGA1UEAxMCUjMwggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEK
AoIBAQC7AhUozPaglNMPEuyNVZLD+ILxmaZ6QoinXSaqtSu5xUyxr45r+XXIo9cP
R5QUVTVXjJ6oojkZ9YI8QqlObvU7wy7bjcCwXPNZOOftz2nwWgsbvsCUJCWH+jdx
sxPnHKzhm+/b5DtFUkWWqcFTzjTIUu61ru2P3mBw4qVUq7ZtDpelQDRrK9O8Zutm
NHz6a4uPVymZ+DAXXbpyb/uBxa3Shlg9F8fnCbvxK/eG3MHacV3URuPMrSXBiLxg
Z3Vms/EY96Jc5lP/Ooi2R6X/ExjqmAl3P51T+c8B5fWmcBcUr2Ok/5mzk53cU6cG
/kiFHaFpriV1uxPMUgP17VGhi9sVAgMBAAGjggEIMIIBBDAOBgNVHQ8BAf8EBAMC
AYYwHQYDVR0lBBYwFAYIKwYBBQUHAwIGCCsGAQUFBwMBMBIGA1UdEwEB/wQIMAYB
Af8CAQAwHQYDVR0OBBYEFBQusxe3WFbLrlAJQOYfr52LFMLGMB8GA1UdIwQYMBaA
FHm0WeZ7tuXkAXOACIjIGlj26ZtuMDIGCCsGAQUFBwEBBCYwJDAiBggrBgEFBQcw
AoYWaHR0cDovL3gxLmkubGVuY3Iub3JnLzAnBgNVHR8EIDAeMBygGqAYhhZodHRw
Oi8veDEuYy5sZW5jci5vcmcvMCIGA1UdIAQbMBkwCAYGZ4EMAQIBMA0GCysGAQQB
gt8TAQEBMA0GCSqGSb3DQEBCwUAA4ICAQCFyk5HPqP3hUSFvNVneLKYY611TR6W
PTNlclQtgaDqw+34IL9fzLdwALduO/ZelN7kIJ+m74uyA+eitRY8kc607TkC53wl
ikfmZW4/RvTZ8M6UK+5UzhK8jCdLuMGYL6KvzXGRSgi3yLgjewQtCPkIVz6D2QQz
CkcheAmCJ8MqyJu5zlzyZMjAvnnAT45tRAxekrsu94sQ4egdRCnbWSDtY7kh+BIm
lJNXoB1lBMEKIq4QDUOXoRgffuDghje1WrG9ML+Hbisq/yFOGwXD9RiX8F6sw6W4
avAuvDszue5L3sz85K+EC4Y/wFVDNvZo4TYXao6Z0f+lQKc0t8DQYzk1OXVu8rp2
yJMC6alLbBfODALZvYH7n7do1AZls4I9d1P4jnkDrQoxB3UqQ9hVl3LEKQ73xF1O
yK5GhDDX8oVfGKF5u+decIsH4YaTw7mP3GFxJSqv3+0lUFJoi5Lc5da149p90Ids
hCExroL1+7mryIkXPeFM5TgO9r0rvZaBFOvV2z0gp35Z0+L4WPlbuEjN/lxPFin+
HlUjr8gRsI3qfJOQFy/9rKIJR0Y/8Omwt/8oTWgy1mdeHmmjk7j1nYsvC9JSQ6Zv
MldlTTKB3zhThV1+XWYp6rjd5JW1zbVWEkLNxE7GJThEUG3szgBVGP7pSWTUTsqX
nLRbwHOoq7hHwg==
-----END CERTIFICATE-----
 2 s:C = US, O = Internet Security Research Group, CN = ISRG Root X1
   i:O = Digital Signature Trust Co., CN = DST Root CA X3
   a:PKEY: rsaEncryption, 4096 (bit); sigalg: RSA-SHA256
   v:NotBefore: Jan 20 19:14:03 2021 GMT; NotAfter: Sep 30 18:14:03 2024 GMT
-----BEGIN CERTIFICATE-----
MIIFYDCCBEigAwIBAgIQQAF3ITfU6UK47naqPGQKtzANBgkqhkiG9w0BAQsFADA/
MSQwIgYDVQQKExtEaWdpdGFsIFNpZ25hdHVyZSBUcnVzdCBDby4xFzAVBgNVBAMT
DkRTVCBSb290IENBIFgzMB4XDTIxMDEyMDE5MTQwM1oXDTI0MDkzMDE4MTQwM1ow
TzELMAkGA1UEBhMCVVMxKTAnBgNVBAoTIEludGVybmV0IFNlY3VyaXR5IFJlc2Vh
cmNoIEdyb3VwMRUwEwYDVQQDEwxJU1JHIFJvb3QgWDEwggIiMA0GCSqGSIb3DQEB
AQUAA4ICDwAwggIKAoICAQCt6CRz9BQ385ueK1coHIe+3LffOJCMbjzmV6B493XC
ov71am72AE8o295ohmxEk7axY/0UEmu/H9LqMZshftEzPLpI9d1537O4/xLxIZpL
wYqGcWlKZmZsj348cL+tKSIG8+TA5oCu4kuPt5l+lAOf00eXfJlII1PoOK5PCm+D
LtFJV4yAdLbaL9A4jXsDcCEbdfIwPPqPrt3aY6vrFk/CjhFLfs8L6P+1dy70sntK
4EwSJQxwjQMpoOFTJOwT2e4ZvxCzSow/iaNhUd6shweU9GNx7C7ib1uYgeGJXDR5
bHbvO5BieebbpJovJsXQEOEO3tkQjhb7t/eo98flAgeYjzYIlefiN5YNNnWe+w5y
sR2bvAP5SQXYgd0FtCrWQemsAXaVCg/Y39W9Eh81LygXbNKYwagJZHduRze6zqxZ
Xmidf3LWicUGQSk+WT7dJvUkyRGnWqNMQB9GoZm1pzpRboY7nn1ypxIFeFntPlF4
FQsDj43QLwWyPntKHEtzBRL8xurgUBN8Q5N0s8p0544fAQjQMNRbcTa0B7rBMDBc
SLeCO5imfWCKoqMpgsy6vYMEG6KDA0Gh1gXxG8K28Kh8hjtGqEgqiNx2mna/H2ql
PRmP6zjzZN7IKw0KKP/32+IVQtQi0Cdd4Xn+GOdwiK1O5tmLOsbdJ1Fu/7xk9TND
TwIDAQABo4IBRjCCAUIwDwYDVR0TAQH/BAUwAwEB/zAOBgNVHQ8BAf8EBAMCAQYw
SwYIKwYBBQUHAQEEPzA9MDsGCCsGAQUFBzAChi9odHRwOi8vYXBwcy5pZGVudHJ1
c3QuY29tL3Jvb3RzL2RzdHJvb3RjYXgzLnA3YzAfBgNVHSMEGDAWgBTEp7Gkeyxx
+tvhS5B1/8QVYIWJEDBUBgNVHSAETTBLMAgGBmeBDAECATA/BgsrBgEEAYLfEwEB
ATAwMC4GCCsGAQUFBwIBFiJodHRwOi8vY3BzLnJvb3QteDEubGV0c2VuY3J5cHQu
b3JnMDwGA1UdHwQ1MDMwMaAvoC2GK2h0dHA6Ly9jcmwuaWRlbnRydXN0LmNvbS9E
U1RST09UQ0FYM0NSTC5jcmwwHQYDVR0OBBYEFHm0WeZ7tuXkAXOACIjIGlj26Ztu
MA0GCSqGSIb3DQEBCwUAA4IBAQAKcwBslm7/DlLQrt2M51oGrS+o44+/yQoDFVDC
5WxCu2+b9LRPwkSICHXM6webFGJueN7sJ7o5XPWioW5WlHAQU7G75K/QosMrAdSW
9MUgNTP52GE24HGNtLi1qoJFlcDyqSMo59ahy2cI2qBDLKobkx/J3vWraV0T9VuG
WCLKTVXkcGdtwlfFRjlBz4pYg1htmf5X6DYO8A4jqv2Il9DjXA6USbW1FzXSLr9O
he8Y4IWS6wY7bCkjCWDcRQJMEhg76fsO3txE+FiYruq9RUWhiF1myv4Q6W+CyBFC
Dfvp7OOGAN6dEOM4+qR9sdjoSYKEBpsr6GtPAQw4dy753ec5
-----END CERTIFICATE-----
---
Server certificate
subject=CN = www.debian.org
issuer=C = US, O = Let's Encrypt, CN = R3
---
No client certificate CA names sent
Peer signing digest: SHA256
Peer signature type: RSA-PSS
Server Temp Key: X25519, 253 bits
---
SSL handshake has read 5085 bytes and written 396 bytes
Verification: OK
---
New, TLSv1.3, Cipher is TLS_AES_256_GCM_SHA384
Server public key is 4096 bit
Secure Renegotiation IS NOT supported
Compression: NONE
Expansion: NONE
No ALPN negotiated
Early data was not sent
Verify return code: 0 (ok)
---

......

HTTP/1.1 400 Bad Request
Date: Sun, 19 Jun 2022 20:02:19 GMT
Server: Apache
X-Content-Type-Options: nosniff
X-Frame-Options: sameorigin
Referrer-Policy: no-referrer
X-Xss-Protection: 1
Permissions-Policy: interest-cohort=()
Strict-Transport-Security: max-age=15552000
Content-Length: 291
Connection: close
Content-Type: text/html; charset=iso-8859-1

<!DOCTYPE HTML PUBLIC "-//IETF//DTD HTML 2.0//EN">
<html><head>
<title>400 Bad Request</title>
</head><body>
<h1>Bad Request</h1>
<p>Your browser sent a request that this server could not understand.<br />
</p>
<hr>
<address>Apache Server at www.debian.org Port 443</address>
</body></html>
closed
 charles   main … 1  …  ses  labs  lab-6  vim co.pem
 charles   main … 2  …  ses  labs  lab-6  vim c1.pem
```

### Step 2: Extract the public key (e, n) from the issuer’s certificate.
Now let's extract our parameters:

```bash
 charles   main ✚ 1 … 3  …  ses  labs  lab-6  openssl x509 -in c1.pem -noout -modulus
Modulus=BB021528CCF6A094D30F12EC8D5592C3F882F199A67A4288A75D26AAB52BB9C54CB1AF8E6BF975C8A3D70F4794145535578C9EA8A23919F5823C42A94E6EF53BC32EDB8DC0B05CF35938E7EDCF69F05A0B1BBEC094242587FA3771B313E71CACE19BEFDBE43B45524596A9C153CE34C852EEB5AEED8FDE6070E2A554ABB66D0E97A540346B2BD3BC66EB66347CFA6B8B8F572999F830175DBA726FFB81C5ADD286583D17C7E709BBF12BF786DCC1DA715DD446E3CCAD25C188BC60677566B3F118F7A25CE653FF3A88B647A5FF1318EA9809773F9D53F9CF01E5F5A6701714AF63A4FF99B3939DDC53A706FE48851DA169AE2575BB13CC5203F5ED51A18BDB15
 charles   main ✚ 1 … 3  …  ses  labs  lab-6  openssl x509 -in c1.pem -noout -text
Certificate:
    Data:
        Version: 3 (0x2)
        Serial Number:
            91:2b:08:4a:cf:0c:18:a7:53:f6:d6:2e:25:a7:5f:5a
        Signature Algorithm: sha256WithRSAEncryption
        Issuer: C = US, O = Internet Security Research Group, CN = ISRG Root X1
        Validity
            Not Before: Sep  4 00:00:00 2020 GMT
            Not After : Sep 15 16:00:00 2025 GMT
        Subject: C = US, O = Let's Encrypt, CN = R3
        Subject Public Key Info:
            Public Key Algorithm: rsaEncryption
                Public-Key: (2048 bit)
                Modulus:
                    00:bb:02:15:28:cc:f6:a0:94:d3:0f:12:ec:8d:55:
                    92:c3:f8:82:f1:99:a6:7a:42:88:a7:5d:26:aa:b5:
                    2b:b9:c5:4c:b1:af:8e:6b:f9:75:c8:a3:d7:0f:47:
                    94:14:55:35:57:8c:9e:a8:a2:39:19:f5:82:3c:42:
                    a9:4e:6e:f5:3b:c3:2e:db:8d:c0:b0:5c:f3:59:38:
                    e7:ed:cf:69:f0:5a:0b:1b:be:c0:94:24:25:87:fa:
                    37:71:b3:13:e7:1c:ac:e1:9b:ef:db:e4:3b:45:52:
                    45:96:a9:c1:53:ce:34:c8:52:ee:b5:ae:ed:8f:de:
                    60:70:e2:a5:54:ab:b6:6d:0e:97:a5:40:34:6b:2b:
                    d3:bc:66:eb:66:34:7c:fa:6b:8b:8f:57:29:99:f8:
                    30:17:5d:ba:72:6f:fb:81:c5:ad:d2:86:58:3d:17:
                    c7:e7:09:bb:f1:2b:f7:86:dc:c1:da:71:5d:d4:46:
                    e3:cc:ad:25:c1:88:bc:60:67:75:66:b3:f1:18:f7:
                    a2:5c:e6:53:ff:3a:88:b6:47:a5:ff:13:18:ea:98:
                    09:77:3f:9d:53:f9:cf:01:e5:f5:a6:70:17:14:af:
                    63:a4:ff:99:b3:93:9d:dc:53:a7:06:fe:48:85:1d:
                    a1:69:ae:25:75:bb:13:cc:52:03:f5:ed:51:a1:8b:
                    db:15
                Exponent: 65537 (0x10001)
        X509v3 extensions:
            X509v3 Key Usage: critical
                Digital Signature, Certificate Sign, CRL Sign
            X509v3 Extended Key Usage: 
                TLS Web Client Authentication, TLS Web Server Authentication
            X509v3 Basic Constraints: critical
                CA:TRUE, pathlen:0
            X509v3 Subject Key Identifier: 
                14:2E:B3:17:B7:58:56:CB:AE:50:09:40:E6:1F:AF:9D:8B:14:C2:C6
            X509v3 Authority Key Identifier: 
                79:B4:59:E6:7B:B6:E5:E4:01:73:80:08:88:C8:1A:58:F6:E9:9B:6E
            Authority Information Access: 
                CA Issuers - URI:http://x1.i.lencr.org/
            X509v3 CRL Distribution Points: 
                Full Name:
                  URI:http://x1.c.lencr.org/
            X509v3 Certificate Policies: 
                Policy: 2.23.140.1.2.1
                Policy: 1.3.6.1.4.1.44947.1.1.1
    Signature Algorithm: sha256WithRSAEncryption
    Signature Value:
        85:ca:4e:47:3e:a3:f7:85:44:85:bc:d5:67:78:b2:98:63:ad:
        75:4d:1e:96:3d:33:65:72:54:2d:81:a0:ea:c3:ed:f8:20:bf:
        5f:cc:b7:70:00:b7:6e:3b:f6:5e:94:de:e4:20:9f:a6:ef:8b:
        b2:03:e7:a2:b5:16:3c:91:ce:b4:ed:39:02:e7:7c:25:8a:47:
        e6:65:6e:3f:46:f4:d9:f0:ce:94:2b:ee:54:ce:12:bc:8c:27:
        4b:b8:c1:98:2f:a2:af:cd:71:91:4a:08:b7:c8:b8:23:7b:04:
        2d:08:f9:08:57:3e:83:d9:04:33:0a:47:21:78:09:82:27:c3:
        2a:c8:9b:b9:ce:5c:f2:64:c8:c0:be:79:c0:4f:8e:6d:44:0c:
        5e:92:bb:2e:f7:8b:10:e1:e8:1d:44:29:db:59:20:ed:63:b9:
        21:f8:12:26:94:93:57:a0:1d:65:04:c1:0a:22:ae:10:0d:43:
        97:a1:18:1f:7e:e0:e0:86:37:b5:5a:b1:bd:30:bf:87:6e:2b:
        2a:ff:21:4e:1b:05:c3:f5:18:97:f0:5e:ac:c3:a5:b8:6a:f0:
        2e:bc:3b:33:b9:ee:4b:de:cc:fc:e4:af:84:0b:86:3f:c0:55:
        43:36:f6:68:e1:36:17:6a:8e:99:d1:ff:a5:40:a7:34:b7:c0:
        d0:63:39:35:39:75:6e:f2:ba:76:c8:93:02:e9:a9:4b:6c:17:
        ce:0c:02:d9:bd:81:fb:9f:b7:68:d4:06:65:b3:82:3d:77:53:
        f8:8e:79:03:ad:0a:31:07:75:2a:43:d8:55:97:72:c4:29:0e:
        f7:c4:5d:4e:c8:ae:46:84:30:d7:f2:85:5f:18:a1:79:bb:e7:
        5e:70:8b:07:e1:86:93:c3:b9:8f:dc:61:71:25:2a:af:df:ed:
        25:50:52:68:8b:92:dc:e5:d6:b5:e3:da:7d:d0:87:6c:84:21:
        31:ae:82:f5:fb:b9:ab:c8:89:17:3d:e1:4c:e5:38:0e:f6:bd:
        2b:bd:96:81:14:eb:d5:db:3d:20:a7:7e:59:d3:e2:f8:58:f9:
        5b:b8:48:cd:fe:5c:4f:16:29:fe:1e:55:23:af:c8:11:b0:8d:
        ea:7c:93:90:17:2f:fd:ac:a2:09:47:46:3f:f0:e9:b0:b7:ff:
        28:4d:68:32:d6:67:5e:1e:69:a3:93:b8:f5:9d:8b:2f:0b:d2:
        52:43:a6:6f:32:57:65:4d:32:81:df:38:53:85:5d:7e:5d:66:
        29:ea:b8:dd:e4:95:b5:cd:b5:56:12:42:cd:c4:4e:c6:25:38:
        44:50:6d:ec:ce:00:55:18:fe:e9:49:64:d4:4e:ca:97:9c:b4:
        5b:c0:73:a8:ab:b8:47:c2
```

From it, we extract the *e*xponent (65537 in decimal).

### Step 3: Extract the signature from the server’s certificate.
Now, let's extract the signature:

```bash
 charles   main ✚ 2 … 5  …  ses  labs  lab-6  openssl x509 -in co.pem -noout -text | tail -n 15 | tr -d '[:space:]:'; echo 
90d0bd5b60965a1cb58323c60eb76a54692eee08708b757a2d1844c56279045366689aaa49c924245343cb148f6bb5600926a60564d712661e277b612aa38eec81a6d202f15d3d45b1205ae09834ce655a90922b69c1cf36e4f7532135e3f6bdc2e412885c8090fe5133c682106e4fc84edf10021daea7ba4898b5ac3a40e0f2b943cf0a3201e01e5cbfbcfb3f26453b2075866f54e3ffbd18d39db1aa06ecddb7ede2e4c694ae39d8a70a0406ace8ab19fd24a373c42e542cc47e87abd39113e4e4961f4305a50dd5ef749926cb9f75caa06ad6ba5d586e09c3979aaa6fb02229bffd8f6bb7b6bd2a676237a3f1a6086857fd7abc32662a98c4905fd9329f80
```

### Step 4: Extract the body of the server’s certificate.
Let's firstly try the `asn1parse` tool:

```bash
 charles   main ✚ 1 … 3  …  ses  labs  lab-6  openssl asn1parse -i -in co.pem
    0:d=0  hl=4 l=1569 cons: SEQUENCE          
    4:d=1  hl=4 l=1289 cons:  SEQUENCE          
    8:d=2  hl=2 l=   3 cons:   cont [ 0 ]        
   10:d=3  hl=2 l=   1 prim:    INTEGER           :02
   13:d=2  hl=2 l=  18 prim:   INTEGER           :0394546A63C04C7346B3447C570DBE9C9263
   33:d=2  hl=2 l=  13 cons:   SEQUENCE          
   35:d=3  hl=2 l=   9 prim:    OBJECT            :sha256WithRSAEncryption
   46:d=3  hl=2 l=   0 prim:    NULL              
   48:d=2  hl=2 l=  50 cons:   SEQUENCE          
   50:d=3  hl=2 l=  11 cons:    SET               
   52:d=4  hl=2 l=   9 cons:     SEQUENCE          
   54:d=5  hl=2 l=   3 prim:      OBJECT            :countryName
   59:d=5  hl=2 l=   2 prim:      PRINTABLESTRING   :US
   63:d=3  hl=2 l=  22 cons:    SET               
   65:d=4  hl=2 l=  20 cons:     SEQUENCE          
   67:d=5  hl=2 l=   3 prim:      OBJECT            :organizationName
   72:d=5  hl=2 l=  13 prim:      PRINTABLESTRING   :Let's Encrypt
   87:d=3  hl=2 l=  11 cons:    SET               
   89:d=4  hl=2 l=   9 cons:     SEQUENCE          
   91:d=5  hl=2 l=   3 prim:      OBJECT            :commonName
   96:d=5  hl=2 l=   2 prim:      PRINTABLESTRING   :R3
  100:d=2  hl=2 l=  30 cons:   SEQUENCE          
  102:d=3  hl=2 l=  13 prim:    UTCTIME           :220425005548Z
  117:d=3  hl=2 l=  13 prim:    UTCTIME           :220724005547Z
  132:d=2  hl=2 l=  25 cons:   SEQUENCE          
  134:d=3  hl=2 l=  23 cons:    SET               
  136:d=4  hl=2 l=  21 cons:     SEQUENCE          
  138:d=5  hl=2 l=   3 prim:      OBJECT            :commonName
  143:d=5  hl=2 l=  14 prim:      PRINTABLESTRING   :www.debian.org
  159:d=2  hl=4 l= 546 cons:   SEQUENCE          
  163:d=3  hl=2 l=  13 cons:    SEQUENCE          
  165:d=4  hl=2 l=   9 prim:     OBJECT            :rsaEncryption
  176:d=4  hl=2 l=   0 prim:     NULL              
  178:d=3  hl=4 l= 527 prim:    BIT STRING        
  709:d=2  hl=4 l= 584 cons:   cont [ 3 ]        
  713:d=3  hl=4 l= 580 cons:    SEQUENCE          
  717:d=4  hl=2 l=  14 cons:     SEQUENCE          
  719:d=5  hl=2 l=   3 prim:      OBJECT            :X509v3 Key Usage
  724:d=5  hl=2 l=   1 prim:      BOOLEAN           :255
  727:d=5  hl=2 l=   4 prim:      OCTET STRING      [HEX DUMP]:030205A0
  733:d=4  hl=2 l=  29 cons:     SEQUENCE          
  735:d=5  hl=2 l=   3 prim:      OBJECT            :X509v3 Extended Key Usage
  740:d=5  hl=2 l=  22 prim:      OCTET STRING      [HEX DUMP]:301406082B0601050507030106082B06010505070302
  764:d=4  hl=2 l=  12 cons:     SEQUENCE          
  766:d=5  hl=2 l=   3 prim:      OBJECT            :X509v3 Basic Constraints
  771:d=5  hl=2 l=   1 prim:      BOOLEAN           :255
  774:d=5  hl=2 l=   2 prim:      OCTET STRING      [HEX DUMP]:3000
  778:d=4  hl=2 l=  29 cons:     SEQUENCE          
  780:d=5  hl=2 l=   3 prim:      OBJECT            :X509v3 Subject Key Identifier
  785:d=5  hl=2 l=  22 prim:      OCTET STRING      [HEX DUMP]:0414C5D3B55B900864745C635FA1AD00B9CA08E3B837
  809:d=4  hl=2 l=  31 cons:     SEQUENCE          
  811:d=5  hl=2 l=   3 prim:      OBJECT            :X509v3 Authority Key Identifier
  816:d=5  hl=2 l=  24 prim:      OCTET STRING      [HEX DUMP]:30168014142EB317B75856CBAE500940E61FAF9D8B14C2C6
  842:d=4  hl=2 l=  85 cons:     SEQUENCE          
  844:d=5  hl=2 l=   8 prim:      OBJECT            :Authority Information Access
  854:d=5  hl=2 l=  73 prim:      OCTET STRING      [HEX DUMP]:3047302106082B060105050730018615687474703A2F2F72332E6F2E6C656E63722E6F7267302206082B060105050730028616687474703A2F2F72332E692E6C656E63722E6F72672F
  929:d=4  hl=2 l=  25 cons:     SEQUENCE          
  931:d=5  hl=2 l=   3 prim:      OBJECT            :X509v3 Subject Alternative Name
  936:d=5  hl=2 l=  18 prim:      OCTET STRING      [HEX DUMP]:3010820E7777772E64656269616E2E6F7267
  956:d=4  hl=2 l=  76 cons:     SEQUENCE          
  958:d=5  hl=2 l=   3 prim:      OBJECT            :X509v3 Certificate Policies
  963:d=5  hl=2 l=  69 prim:      OCTET STRING      [HEX DUMP]:30433008060667810C0102013037060B2B0601040182DF130101013028302606082B06010505070201161A687474703A2F2F6370732E6C657473656E63727970742E6F7267
 1034:d=4  hl=4 l= 259 cons:     SEQUENCE          
 1038:d=5  hl=2 l=  10 prim:      OBJECT            :CT Precertificate SCTs
 1050:d=5  hl=3 l= 244 prim:      OCTET STRING      [HEX DUMP]:0481F100EF007500DFA55EAB68824F1F6CADEEB85F4E3E5AEACDA212A46A5E8E3B12C020445C2A73000001805E6E9A5F0000040300463044022045605A77927E49CF3C854416B9F13B5A0B8F4A34916BBA731AA8167C45DBB97402206633EE55F9D65C00E9D9E22FE951A033A5318A1A179401008C00A828D0DDB28C0076002979BEF09E393921F056739F63A577E5BE577D9C600AF8F94D5D265C255DC784000001805E6E9A58000004030047304502210082439469BA5E1DA26769354C740AFF6B5270FFB0D9E43C82729B6AF7481AF83002206E348580F04E56C74279333FE50863890C918D608581490A201FC00122DB17E6
 1297:d=1  hl=2 l=  13 cons:  SEQUENCE          
 1299:d=2  hl=2 l=   9 prim:   OBJECT            :sha256WithRSAEncryption
 1310:d=2  hl=2 l=   0 prim:   NULL              
 1312:d=1  hl=4 l= 257 prim:  BIT STRING        
```

From this, we can see that our *body* starts in offset 4 and goes until
offset 1297. And let's use `-strparse` option to give us only the *body*
and hash it:

```bash
 charles   main ✚ 1 … 4  …  ses  labs  lab-6  openssl asn1parse -i -in co.pem -strparse 4 -out co_body.bin -noout
 charles   main ✚ 1 … 5  …  ses  labs  lab-6  sha256sum co_body.bin 
15488f67c8c39fd80d3b93a4d3f55c208a2edff884daf5b443633cc7a0132bf6  co_body.bin
```

### Step 5: Verify the signature.
Now let's use our program to verify it's integrity.

```c
void task6() {
	BN_CTX *ctx = BN_CTX_new();
	BIGNUM *n = BN_new();
	BIGNUM *e = BN_new();
	BIGNUM *plaintext_to_sign = BN_new();
	BIGNUM *sign_generated = BN_new();
	BIGNUM *signature = BN_new();

	// Initialize p, q, e
	BN_hex2bn(&n, "BB021528CCF6A094D30F12EC8D5592C3F882F199A67A4288A75D26AAB52BB9C54CB1AF8E6BF975C8A3D70F4794145535578C9EA8A23919F5823C42A94E6EF53BC32EDB8DC0B05CF35938E7EDCF69F05A0B1BBEC094242587FA3771B313E71CACE19BEFDBE43B45524596A9C153CE34C852EEB5AEED8FDE6070E2A554ABB66D0E97A540346B2BD3BC66EB66347CFA6B8B8F572999F830175DBA726FFB81C5ADD286583D17C7E709BBF12BF786DCC1DA715DD446E3CCAD25C188BC60677566B3F118F7A25CE653FF3A88B647A5FF1318EA9809773F9D53F9CF01E5F5A6701714AF63A4FF99B3939DDC53A706FE48851DA169AE2575BB13CC5203F5ED51A18BDB15");
	BN_hex2bn(&e, "010001");
	BN_hex2bn(&plaintext_to_sign, "15488F67C8C39FD80D3B93A4D3F55C208A2EDFF884DAF5B443633CC7A0132BF6");
	BN_hex2bn(&signature, "90D0BD5B60965A1CB58323C60EB76A54692EEE08708B757A2D1844C56279045366689AAA49C924245343CB148F6BB5600926A60564D712661E277B612AA38EEC81A6D202F15D3D45B1205AE09834CE655A90922B69C1CF36E4F7532135E3F6BDC2E412885C8090FE5133C682106E4FC84EDF10021DAEA7BA4898B5AC3A40E0F2B943CF0A3201E01E5CBFBCFB3F26453B2075866F54E3FFBD18D39DB1AA06ECDDB7EDE2E4C694AE39D8A70A0406ACE8AB19FD24A373C42E542CC47E87ABD39113E4E4961F4305A50DD5EF749926CB9F75CAA06AD6BA5D586E09C3979AAA6FB02229BFFD8F6BB7B6BD2A676237A3F1A6086857FD7ABC32662A98C4905FD9329F80");

	// encryption: sign_generated = signature^d mod n
	BN_mod_exp(sign_generated, signature, e, n, ctx);

	char *input_hash = BN_bn2hex(plaintext_to_sign);
	char *computed_hash = BN_bn2hex(sign_generated);
	unsigned long input_hash_len = strlen(input_hash);
	unsigned long computed_hash_len = strlen(computed_hash);

	printf("Input hash size: %zu\nInput hash: %s\n", strlen(input_hash), input_hash);
	printf("Generated hash size: %zu\nGenerated hash: %s\n", strlen(computed_hash), computed_hash);
	
	int is_equal = 0;

	for(int i = 0; i < input_hash_len; i++) {
		is_equal = input_hash[input_hash_len - i] == computed_hash[computed_hash_len - i];
		if (!is_equal)
			break;
	}
			
	if (is_equal) {
		printf("Message verified!\n");
	} else {
		printf("Message corrupted!\n");
	}
}
```

And let's execute it:

```bash
 charles   main ✚ 2 … 5  …  ses  labs  lab-6  ./rsa 
Input hash size: 64
Input hash: 15488F67C8C39FD80D3B93A4D3F55C208A2EDFF884DAF5B443633CC7A0132BF6
Generated hash size: 510
Generated hash: 01FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF003031300D06096086480165030402010500042015488F67C8C39FD80D3B93A4D3F55C208A2EDFF884DAF5B443633CC7A0132BF6
Message verified!
```

As we can see, our program verified it. The only point to discuss is a minor
alteration because the signature from the certificate comes padded. That's
why we have to compare only the last 32 bytes (or 64 characters because of
hex) of it with our computed hash.

## Conclusion
In this lab we were able to learn in practice and getting our hands dirty
how the rsa encription, decription and signature validation works.
