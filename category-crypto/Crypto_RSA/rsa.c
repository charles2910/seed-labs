/* bn_sample.c */
#include <stdio.h>
#include <openssl/bn.h>
#include <string.h>

#define NBITS 256

void printBN(char *msg, BIGNUM * a)
{
	/* Use BN_bn2hex(a) for hex string
	 * Use BN_bn2dec(a) for decimal string */
	char * number_str = BN_bn2hex(a);
	printf("%s %s\n", msg, number_str);
	OPENSSL_free(number_str);
}

void task6() {
	BN_CTX *ctx = BN_CTX_new();
	BIGNUM *n = BN_new();
	BIGNUM *e = BN_new();
	BIGNUM *plaintext_to_sign = BN_new();
	BIGNUM *sign_generated = BN_new();
	BIGNUM *signature = BN_new();

	// Initialize p, q, e
	BN_hex2bn(&n, "BB021528CCF6A094D30F12EC8D5592C3F882F199A67A4288A75D26AAB52BB9C54CB1AF8E6BF975C8A3D70F4794145535578C9EA8A23919F5823C42A94E6EF53BC32EDB8DC0B05CF35938E7EDCF69F05A0B1BBEC094242587FA3771B313E71CACE19BEFDBE43B45524596A9C153CE34C852EEB5AEED8FDE6070E2A554ABB66D0E97A540346B2BD3BC66EB66347CFA6B8B8F572999F830175DBA726FFB81C5ADD286583D17C7E709BBF12BF786DCC1DA715DD446E3CCAD25C188BC60677566B3F118F7A25CE653FF3A88B647A5FF1318EA9809773F9D53F9CF01E5F5A6701714AF63A4FF99B3939DDC53A706FE48851DA169AE2575BB13CC5203F5ED51A18BDB15");
	BN_hex2bn(&e, "010001");
	BN_hex2bn(&plaintext_to_sign, "15488F67C8C39FD80D3B93A4D3F55C208A2EDFF884DAF5B443633CC7A0132BF6");
	BN_hex2bn(&signature, "90D0BD5B60965A1CB58323C60EB76A54692EEE08708B757A2D1844C56279045366689AAA49C924245343CB148F6BB5600926A60564D712661E277B612AA38EEC81A6D202F15D3D45B1205AE09834CE655A90922B69C1CF36E4F7532135E3F6BDC2E412885C8090FE5133C682106E4FC84EDF10021DAEA7BA4898B5AC3A40E0F2B943CF0A3201E01E5CBFBCFB3F26453B2075866F54E3FFBD18D39DB1AA06ECDDB7EDE2E4C694AE39D8A70A0406ACE8AB19FD24A373C42E542CC47E87ABD39113E4E4961F4305A50DD5EF749926CB9F75CAA06AD6BA5D586E09C3979AAA6FB02229BFFD8F6BB7B6BD2A676237A3F1A6086857FD7ABC32662A98C4905FD9329F80");

	// encryption: sign_generated = signature^d mod n
	BN_mod_exp(sign_generated, signature, e, n, ctx);

	char *input_hash = BN_bn2hex(plaintext_to_sign);
	char *computed_hash = BN_bn2hex(sign_generated);
	unsigned long input_hash_len = strlen(input_hash);
	unsigned long computed_hash_len = strlen(computed_hash);

	printf("Input hash size: %zu\nInput hash: %s\n", strlen(input_hash), input_hash);
	printf("Generated hash size: %zu\nGenerated hash: %s\n", strlen(computed_hash), computed_hash);
	
	int is_equal = 0;

	for(int i = 0; i < input_hash_len; i++) {
		is_equal = input_hash[input_hash_len - i] == computed_hash[computed_hash_len - i];
		if (!is_equal)
			break;
	}
			
	if (is_equal) {
		printf("Message verified!\n");
	} else {
		printf("Message corrupted!\n");
	}
}

void task5a() {
	BN_CTX *ctx = BN_CTX_new();
	BIGNUM *n = BN_new();
	BIGNUM *e = BN_new();
	BIGNUM *plaintext_to_sign = BN_new();
	BIGNUM *sign_generated = BN_new();
	BIGNUM *signature = BN_new();

	// Initialize p, q, e
	BN_hex2bn(&n, "AE1CD4DC432798D933779FBD46C6E1247F0CF1233595113AA51B450F18116115");
	BN_hex2bn(&e, "010001");
	BN_hex2bn(&plaintext_to_sign, "4C61756E63682061206D697373696C652E");
	BN_hex2bn(&signature, "643D6F34902D9C7EC90CB0B2BCA36C47FA37165C0005CAB026C0542CBDB6803F");

	// encryption: sign_generated = signature^d mod n
	BN_mod_exp(sign_generated, signature, e, n, ctx);
	printBN("Signature = ", plaintext_to_sign);
	printBN("Signature expected = ", sign_generated);

	if (BN_cmp(plaintext_to_sign, sign_generated) == 0) {
		printf("Message verified!\n");
	} else {
		printf("Message corrupted!\n");
	}
}

void task5() {
	BN_CTX *ctx = BN_CTX_new();
	BIGNUM *n = BN_new();
	BIGNUM *e = BN_new();
	BIGNUM *plaintext_to_sign = BN_new();
	BIGNUM *sign_generated = BN_new();
	BIGNUM *signature = BN_new();

	// Initialize p, q, e
	BN_hex2bn(&n, "AE1CD4DC432798D933779FBD46C6E1247F0CF1233595113AA51B450F18116115");
	BN_hex2bn(&e, "010001");
	BN_hex2bn(&plaintext_to_sign, "4C61756E63682061206D697373696C652E");
	BN_hex2bn(&signature, "643D6F34902D9C7EC90CB0B2BCA36C47FA37165C0005CAB026C0542CBDB6802F");

	// encryption: sign_generated = signature^d mod n
	BN_mod_exp(sign_generated, signature, e, n, ctx);
	printBN("Signature = ", plaintext_to_sign);
	printBN("Signature expected = ", sign_generated);

	if (BN_cmp(plaintext_to_sign, sign_generated) == 0) {
		printf("Message verified!\n");
	} else {
		printf("Message corrupted!\n");
	}
}

void task4a() {
	BN_CTX *ctx = BN_CTX_new();
	BIGNUM *n = BN_new();
	BIGNUM *d = BN_new();
	BIGNUM *plaintext_to_sign = BN_new();
	BIGNUM *signature = BN_new();

	// Initialize p, q, d
	BN_hex2bn(&n, "DCBFFE3E51F62E09CE7032E2677A78946A849DC4CDDE3A4D0CB81629242FB1A5");
	BN_hex2bn(&d, "74D806F9F3A62BAE331FFE3F0A68AFE35B3D2E4794148AACBC26AA381CD7D30D");
	BN_hex2bn(&plaintext_to_sign, "4d203d2049206f776520796f752024333030302e");

	// encryption: signature =  plaintext_to_sign^d mod n
	BN_mod_exp(signature, plaintext_to_sign, d, n, ctx);
	printBN("Signature = ", signature);
}

void task4() {
	BN_CTX *ctx = BN_CTX_new();
	BIGNUM *n = BN_new();
	BIGNUM *d = BN_new();
	BIGNUM *plaintext_to_sign = BN_new();
	BIGNUM *signature = BN_new();

	// Initialize p, q, d
	BN_hex2bn(&n, "DCBFFE3E51F62E09CE7032E2677A78946A849DC4CDDE3A4D0CB81629242FB1A5");
	BN_hex2bn(&d, "74D806F9F3A62BAE331FFE3F0A68AFE35B3D2E4794148AACBC26AA381CD7D30D");
	BN_hex2bn(&plaintext_to_sign, "4d203d2049206f776520796f752024323030302e");

	// encryption: signature =  plaintext_to_sign^d mod n
	BN_mod_exp(signature, plaintext_to_sign, d, n, ctx);
	printBN("Signature = ", signature);
}

void task3() {
	BN_CTX *ctx = BN_CTX_new();
	BIGNUM *n = BN_new();
	BIGNUM *d = BN_new();
	BIGNUM *plaintext = BN_new();
	BIGNUM *ciphertext = BN_new();

	// Initialize p, q, d
	BN_hex2bn(&n, "DCBFFE3E51F62E09CE7032E2677A78946A849DC4CDDE3A4D0CB81629242FB1A5");
	BN_hex2bn(&d, "74D806F9F3A62BAE331FFE3F0A68AFE35B3D2E4794148AACBC26AA381CD7D30D");
	BN_hex2bn(&ciphertext, "8C0F971DF2F3672B28811407E2DABBE1DA0FEBBBDFC7DCB67396567EA1E2493F");

	// encryption: plaintext =  ciphertext^d mod n
	BN_mod_exp(plaintext, ciphertext, d, n, ctx);
	printBN("Plaintext = ", plaintext);
}

void task2() {
	BN_CTX *ctx = BN_CTX_new();
	BIGNUM *n = BN_new();
	BIGNUM *e = BN_new();
	BIGNUM *plaintext = BN_new();
	BIGNUM *ciphertext = BN_new();

	// Initialize p, q, e and one
	BN_hex2bn(&n, "DCBFFE3E51F62E09CE7032E2677A78946A849DC4CDDE3A4D0CB81629242FB1A5");
	BN_hex2bn(&e, "010001");
	BN_hex2bn(&plaintext, "4120746f702073656372657421");

	// encryption: ciphertext =  plaintext^e mod n
	BN_mod_exp(ciphertext, plaintext, e, n, ctx);
	printBN("Ciphertext = ", ciphertext);
}

void task1() {
	BN_CTX *ctx = BN_CTX_new();
	BIGNUM *p = BN_new();
	BIGNUM *pl1 = BN_new();
	BIGNUM *q = BN_new();
	BIGNUM *ql1 = BN_new();
	BIGNUM *e = BN_new();
	BIGNUM *t = BN_new();
	BIGNUM *d = BN_new();
	BIGNUM *one = BN_new();

	// Initialize p, q, e and one
	BN_one(one);
	BN_hex2bn(&p, "F7E75FDC469067FFDC4E847C51F452DF");
	BN_hex2bn(&q, "E85CED54AF57E53E092113E62F436F4F");
	BN_hex2bn(&e, "0D88C3");

	// t = totient(n) = (p - 1)*(q - 1)
	BN_sub(pl1, p, one);
	BN_sub(ql1, q, one);
	BN_mul(t, pl1, ql1, ctx);
	// d = modinv(e, t)
	BN_mod_inverse(d, e, t, ctx);
	printBN("d = ", d);
}

void task0() {
	BN_CTX *ctx = BN_CTX_new();
	BIGNUM *a = BN_new();
	BIGNUM *b = BN_new();
	BIGNUM *n = BN_new();
	BIGNUM *res = BN_new();

	// Initialize a, b, n
	BN_generate_prime_ex(a, NBITS, 1, NULL, NULL, NULL);
	BN_dec2bn(&b, "273489463796838501848592769467194369268");
	BN_rand(n, NBITS, 0, 0);

	// res = a*b
	BN_mul(res, a, b, ctx);
	printBN("a * b = ", res);

	// res = aˆb mod n
	BN_mod_exp(res, a, b, n, ctx);
	printBN("aˆc mod n = ", res);
}

int main()
{
	task6();

	return 0;
}
